(ns advent-of-code-2019.day10
  (:require [clojure.string :as s]))

(defn parse-asteroid-map [asteroid-map]
  (map #(s/split % #"") (s/split-lines asteroid-map)))

(defn count-individual-los [asteroid-map [x y]])
  

(defn find-los [asteroid-map [x y]]
  (if (= "." (get-in asteroid-map [x y]))
    asteroid-map
    (assoc-in asteroid-map [x y] (count-individual-los asteroid-map [x y]))))

(defn count-los [asteroid-map]
  (let [ast-map (parse-asteroid-map asteroid-map)
        rows    (count ast-map)
        cols    (count (first ast-map))]
    (find-los ast-map [rows cols])))
      
(comment
  (do
        (clojure.tools.namespace.repl/refresh)
        (run-tests 'advent-of-code-2019.core-test))
  (def example-map-1
    ".#..#\n.....\n#####\n....#\n...##")

  (def count-los-map-1
    ".7..7\n.....\n67775\n....7\n...87")

  (count-los example-map-1))
