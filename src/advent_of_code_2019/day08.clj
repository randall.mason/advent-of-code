(ns advent-of-code-2019.day08
  (:require [clojure.string :as s]))

(def width 25)
(def height 6)
(def area (* width height))

(defn min-layer [input number]
  (first (apply min-key val
                (into {}
                      (map-indexed hash-map
                                   (map #(% number)
                                        (map frequencies
                                             (partition area input))))))))

(defn answer1 [input]
 (let [input-ints (map read-string (s/split (s/trim input) #""))]
  (reduce *
   ((juxt (comp count (partial filter #(= 2 %)))
          (comp count (partial filter #(= 1 %))))
    (nth (into [] (partition area input-ints)) (min-layer input-ints 0))))))
    
(defn answer2 [input]
 (let [input-ints (map read-string (s/split (s/trim input) #""))]
    (partition width (map (fn [x] (first (filter #(not= 2 %) (map #(nth % x) (partition area input-ints))))) (range area)))))

(comment
  (do
    (require '[clojure.tools.namespace.repl])
    (require '[clojure.test :refer [run-tests]] :reload-all)
    (require '[advent-of-code-2019.core-test])
    (def input-ints (map read-string (s/split (s/trim (slurp "resources/2019/day08.txt")) #"")))
    (do
      (clojure.tools.namespace.repl/refresh)
      (run-tests 'advent-of-code-2019.core-test))))
