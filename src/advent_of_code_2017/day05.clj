(ns advent-of-code-2017.day05
  (:require [clojure.string :as s]
            [clojure.pprint :as pp]
            [clojure.walk :as w]))

(defn char-to-int [char]
  (- (int char) 48))

(defn string-to-int [string]
  (biginteger ^String string))

(defn advent5-1
 ([code]
  (->> (s/split-lines code)
       (map bigint)
       (apply vector)
       (advent5-1 0 0)))
 ([pos jumps code]
  ;(println pos jumps code)
  (if (<= (count code) (+ pos (nth code pos)))
      (inc jumps)
      (recur (+ pos (nth code pos)) (inc jumps) (update-in code [pos] inc)))))

(defn advent5-2
 ([code]
  (->> (s/split-lines code)
       (map bigint)
       (apply vector)
       (advent5-2 0 0)))
 ([pos jumps code]
  ;(println pos jumps code)
  (if (<= (count code) (+ pos (nth code pos)))
      (inc jumps)
      (recur (+ pos (nth code pos)) (inc jumps) (update-in code [pos] (if (> 3 (nth code pos)) inc dec))))))
