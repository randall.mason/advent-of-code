(ns advent-of-code-2015.day03
  (:require [clojure.string :as s]
            [clojure.pprint :as pp]))
            ;; [uncomplicate.neanderthal.core   :as c]
            ;; [uncomplicate.neanderthal.native :as n]))

(def direction-map
  {">" [ 1  0]
   "<" [-1  0]
   "v" [ 0 -1]
   "^" [ 0  1]})

(defn parse-map [map-string]
  (replace direction-map (s/split map-string #"")))

(deftype cord [^Integer north
               ^Integer east])

(defmethod print-method cord [v ^java.io.Writer w]
  (.write w (str (.north v) " north, " (.east v) " east")))

(defn plus
  ([] (cord. 0 0))
  ([^cord c1]
   c1)
  ([^cord c1 ^cord c2]
   (cord. (+ (.north c1) (.north c2))
          (+ (.east  c1) (.east  c2))))
  ([^cord c1 ^cord c2 & more]
   (reduce plus (plus c1 c2) more)))

(def houses (atom {{:x 0 :y 0} 1}))

(defn visit-houses
 ([map location]
  (let [location     (cord. (first location) (second location))
        parsed-map   (parse-map map)
        new-location (first parsed-map)]
    (println map parsed-map new-location)
    new-location)))

(defn count-houses [visited-houses]
  (println visited-houses)
  (count visited-houses))
