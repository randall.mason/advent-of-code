(ns advent-of-code-2017.day10
  (:require [clojure.string :as s]
            [clojure.pprint :as pp]
            [clojure.walk :as w]))

(defn parse-input [str]
  (map int str))

(def input-tail
  [17, 31, 73, 47, 23])

(defn xor-bytes [bytes]
  (reduce bit-xor bytes))

(defn bytes-to-string [bytes]
  (apply str (map (partial format "%02x") bytes)))

(defn reverse-section [the-list length position]
    ;(println length position)
    #_(println ;(take position inf-list)
             (reverse (take length (drop position inf-list)))
             (take 20 (drop (+ position length) inf-list))
             (take total-length (concat (take position inf-list)
                                        (reverse (take length (drop position inf-list)))
                                        (drop (+ position length) inf-list))))
    (let [inf-list (cycle the-list)
          total-length (count the-list)]
      (cond
        (= 1 length)
        (take total-length inf-list)
        (= length total-length)
        (take total-length (drop (- total-length position)
                                 (cycle (reverse (take length (drop position inf-list))))))
        (<= (+ position length) total-length)
        (take total-length (concat
                             (take position inf-list)
                             (reverse (take length (drop position inf-list)))
                             (drop (+ position length) inf-list)))
        ;:else (println (take total-length inf-list) length position total-length)
        :else
        (take total-length
          (drop (- total-length position)
           (cycle (take total-length (concat
                                      (reverse (take length (drop position inf-list)))
                                      (drop (+ position length) inf-list)))))))))

(defn knot-hash
  ([length jumps]
   ;(println (map bigint (s/split jumps #",")))
   (knot-hash (range length) (map bigint (s/split jumps #",")) 0 0))
  ([circle-list jumps-left position skip-size]
   (if (= 0 (count jumps-left))
       circle-list
       (let [new-jumps-left (rest jumps-left)
             new-position   (mod (+ position (first jumps-left) skip-size) (count circle-list))
             new-skip-size  (inc skip-size)]
         ;(println circle-list jumps-left position skip-size)
         (recur (reverse-section circle-list (first jumps-left) position)
           (rest jumps-left)
           (mod (+ position (first jumps-left) skip-size) (count circle-list))
           (inc skip-size))))))

(defn multi-round-knot-hash [rounds salt]
  ;(println (s/trim salt))
  (map xor-bytes (partition 16 (knot-hash (range 256) (flatten (repeat 64 (concat (parse-input salt) input-tail))) 0 0))))
