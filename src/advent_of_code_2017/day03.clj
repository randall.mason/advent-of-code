(ns advent-of-code-2017.day03
  (:require [clojure.string :as s]
            [clojure.pprint :as pp]
            [clojure.walk :as w]))

(defn char-to-int [char]
  (- (int char) 48))

(defn string-to-int [string]
  (biginteger ^String string))

(def sides (cons 0 (range 1 Double/POSITIVE_INFINITY 2)))
(def rings (map #(* % %) sides))

(defn ring [element]
  (dec (count (take-while (partial > element) rings))))

(defn last-ring-el [element]
  (nth rings (inc (ring element))))

(defn first-ring-el [element]
  (inc (nth rings (ring element))))

(defn center-elements [element]
 (let [max (last-ring-el element)
       ring (ring element)
       side (dec (int (Math/sqrt max)))]
  [(- max ring) (- max ring side) (- max ring side side) (- max ring side side side)]))
  ; (map (nth sides ring))
  ; (int (/ (nth sides ring) 2))))

(defn advent3-1 [element]
  (+ (ring element)
     (apply min (map (comp #(Math/abs %) (partial - element)) (center-elements element)))))

(def point0
  {:el  0
   :x   0
   :y   0
   :val 1
   :dir :x-pos
   :ring 0})

(def point1
  {:el  1
   :x   1
   :y   0
   :val 1
   :dir :y-pos
   :ring 1})

(def point2
  {:el 2
    :x 1
    :y 1
    :val 2
    :dir :x-neg
    :ring 1})

(def point3
  {:el 3
   :x  0
   :y  1
   :val 4
   :dir :x-neg
   :ring 1})

(def point4
  {:el 4
   :x  -1
   :y  1
   :val 5
   :dir :x-neg
   :ring 1})

(def directions
  {:right-side
    [[-1 -1]
     [-1 0]
     [0 -1]
     [-1 1]]
   :left-side
    [[1 1]
     [1 0]
     [1 -1]
     [0 1]]
   :top-side
    [[1 0]
     [-1 -1]
     [0 -1]
     [1 -1]]
   :bottom-side
    [[1 1]
     [0 1]
     [-1 1]
     [-1 0]]})

(def side-order [:right-side :top-side :left-side :bottom-side])

(defn next-side [side]
  (drop-while #(not (side %)) (cycle side-order)))
(def movement-direction
  {:right-side
    [0 1]
   :left-side
    [0 -1]
   :top-side
    [-1 0]
   :bottom-side
    [1 0]})

(defn next-point [x] (cycle [1 2 3 4 5 361528]))

(defn points [first-point]
  ;(lazy-seq
    (next-point first-point))

(defn advent3-2 [value])
  ;(drop-while (partial > value) (points {:number 0 :location [0 0] :value 1})))
