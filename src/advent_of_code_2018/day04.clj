(ns advent-of-code-2018.day04
  (:require [clojure.test :refer :all]
            [clojure.string :as s]
            [clojure.core.reducers :as r]))

(def events
  {"falls asleep" :falls-alseep
   "wakes up" :wakes-up
   "begins shift" :begins-shift})

(defn parse-event [event]
  (let [match (re-matches #"Guard #(\d+) begins shift" event)]
    (if (not (nil? match))
      {:event :begins-shift :guard (read-string (last match))}
      {:event (events event)})))

(defn fill-in-guards [schedule]
  (let [guard (atom 0)]
    (for [event schedule]
      (if (:guard event)
        (do
          (reset! guard (:guard event))
          event)
        (assoc event :guard @guard)))))

(defn parse-schedule [schedule-strings]
  (->> schedule-strings
      s/split-lines
      (map s/trim)
      sort
      (map (partial re-matches  #"\[(\d+)-0?(\d+)-0?(\d+) 0?(\d+):0?(\d+)\] (.*)"))
      (map #(flatten (list (first %) (map read-string (take 5 (rest %))) (last %))))
      (map (fn [claim]
             (into {}
                   (map #(hash-map %1 %2)
                        [:orig :year :month :day :hour :minute :event]
                        claim))))
      (map #(merge % (parse-event (:event %))))
      fill-in-guards))

(defn update-result [result line]
  (let [result (update-in result
                          [(:guard line) :minutes-asleep]
                          (fnil (partial + (- (:minute line) (:minute (:asleep result)))) 0))
        result (update-in result
                          [(:guard line) :sleep]
                          (fnil #(conj % (:minute (:asleep result)) (:minute line)) []))]
    result))

(defn accumulate-hours-by-guard [result line]
  (case (:event line)
    :falls-alseep (assoc result :asleep line)
    :wakes-up     (update-result result line)
    result))

(defn sleep-wake [sleeps]
  (frequencies (flatten (for [[start end] (partition 2 sleeps)]
                         (for [minute (range start end)]
                           minute)))))

(defn accumulate-minutes-for-guard [guard-hours]
  (key (apply max-key val (sleep-wake (get-in guard-hours [:guard-sleep (:guard guard-hours) :sleep])))))

(defn most-hours-asleep [schedule]
  (let [sleep (dissoc (reduce accumulate-hours-by-guard {} schedule) :asleep)]
    {:guard (key (apply max-key
                        (comp :minutes-asleep val)
                        sleep))
     :guard-sleep sleep
     :schedule schedule}))

(defn most-asleep [guard-hours]
  (* (:guard guard-hours) (accumulate-minutes-for-guard guard-hours)))

(defn sleep-wake-global [[guard sleeps]]
  {guard
   (apply max-key val (sleep-wake (:sleep sleeps)))})

(defn most-regular-sleeper [schedule]
  (let [sleep (dissoc (reduce accumulate-hours-by-guard {} schedule) :asleep)
        sleep-global (apply merge (map sleep-wake-global sleep))]
    {:guard (apply max-key (comp val second) sleep-global)
     :guard-sleep sleep
     :schedule schedule}))
