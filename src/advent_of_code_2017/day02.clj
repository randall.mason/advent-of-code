(ns advent-of-code-2017.day02
  (:require [clojure.string :as s]
            [clojure.pprint :as pp]
            [clojure.walk :as w]))

(defn char-to-int [char]
  (- (int char) 48))

(defn string-to-int [string]
  (biginteger ^String string))

(defn split-and-int [spreadsheet]
  (->> spreadsheet
    s/split-lines
    (map #(s/split % #"\s"))
    (map #(map (comp string-to-int) %))))

(defn advent2-1 [spreadsheet]
  (->> spreadsheet
    split-and-int
    (map #(vector (apply max %) (apply min %)))
    (map #(reduce - %))
    (reduce +)))


(defn advent2-2 [spreadsheet]
  (as-> spreadsheet s
    (split-and-int s)
    (map (fn [row]
           (map (fn [el]
                  (map #(/ el %) row)) row)) s)
    (map-indexed (fn [row-number row]
                    (map-indexed #(vector %1
                                   (keep-indexed (fn [i el]
                                                  (if (not (or (ratio? el)
                                                               (= 1 el)))
                                                      (vector i el)))
                                                 %2)) row)) s)
   (map (fn [row] (filter #(not (empty? (nth % 1))) row)) s)
   (map (fn [row] (nth (first (nth (first row) 1)) 1)) s)
   (reduce + s)))
