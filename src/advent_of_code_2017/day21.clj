(ns advent-of-code-2017.day21
  (:require [clojure.string :as s]
            [clojure.pprint :as pp]))

(defn parse [art]
  (partition 3 4 (map #(if (= % "#") 1 0) (s/split art #""))))

(defn normalize [art]
  (pp/pprint (parse art))
  (println (map (partial reduce +) (parse art)))

  (pp/pprint (apply map vector (parse art)))
  (println (map (partial reduce +) (apply map vector (parse art))))

  (pp/pprint (apply map vector (apply map vector (parse art))))
  (println (map (partial reduce +) (apply map vector (apply map vector (parse art)))))

  (pp/pprint (apply map vector (apply map vector (apply map vector (parse art)))))
  (println (map (partial reduce +) (apply map vector (apply map vector (apply map vector (parse art))))))

  (println "")
  art)

(defn parse-rules [rule-string]
  (pp/pprint rule-string)
  (->>
    rule-string
    s/split-lines
    (map #(s/split % #" => "))
    (map #(vector (normalize (first %)) (second %)))
    (apply vector)
    (first)))

(defn generate [rules art steps]
  (if (= 0 steps)
      art
      (do
        (parse-rules rules))))

(defn count-pixels [art]
  art)
  ;(count (s/replace art #"[^#]" "")))
