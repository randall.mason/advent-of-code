(ns advent-of-code-2018.day02
  (:require [clojure.string :as s]
            [clojure.data :as d]))

; https://stackoverflow.com/a/18993290
(defn find-in [weights item]
  (for [[x row] (map-indexed vector weights)
        [y val] (map-indexed vector row)
        :when (= item val)]
     [x y]))

; Roseta code
(defn levenshtein [str1 str2]
  (let [len1 (count str1)
        len2 (count str2)]
    (cond (zero? len1) len2
          (zero? len2) len1
          :else
          (let [cost (if (= (first str1) (first str2)) 0 1)]
            (min (+ 8 (levenshtein (rest str1) str2))
                 (+ 8 (levenshtein str1 (rest str2)))
                 (+ cost
                    (levenshtein (rest str1) (rest str2))))))))

(defn simple-cost [str1 str2]
  (reduce + (map #(if (= %1 %2) 0 1) str1 str2)))

(defn boxid-freqs [boxids]
  (map frequencies boxids))

(defn checksum-boxids [boxids]
  (let [freqs (boxid-freqs boxids)]
    (*
     (count (filter #(some (partial = 2) (map second %)) freqs))
     (count (filter #(some (partial = 3) (map second %)) freqs)))))

(defn parse-boxids [boxid-strings]
  (map s/trim (s/split-lines boxid-strings)))

(defn find-similar [boxids]
  (let [[closest]
        (find-in
         (map
           #(map (partial simple-cost (nth boxids %)) (rest (drop % boxids))) (range (count boxids)))
         1)]
   [(nth boxids (first closest))
    (nth boxids (+ 1 (second closest) (first closest)))]))

(defn diff-similar [boxids]
  (s/join "" (map second
              (filter #(= (nth (second boxids) (first %)) (second %)) (map-indexed vector (first boxids))))))
