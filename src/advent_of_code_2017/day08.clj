(ns advent-of-code-2017.day08
  (:require [clojure.string :as s]
            [clojure.pprint :as pp]
            [clojure.walk :as w]))

(def cond-transform
  {">"  >
   "<"  <
   "==" =
   ">=" >=
   "<=" <=
   "!=" not=})

(def op-transform
  {:inc +
   :dec -})

(defn data-all-the-things [instruction-line]
  (let [[_ reg-target operation amount _ reg-cond conditional conditional-predicate] (re-find #"([^ ]+) ([^ ]+) ([^ ]+) ([^ ]+) ([^ ]+) ([^ ]+) ([^ ]+)" instruction-line)]
       {:reg-target (keyword reg-target)
        :operation  ((keyword operation) op-transform)
        :amount     (bigint amount)
        :reg-cond   (keyword reg-cond)
        :cond       (get cond-transform conditional conditional)
        :cond-pred  (bigint conditional-predicate)}))

(defn parse-8 [instruction-string]
  (->> instruction-string
    s/split-lines
    (map s/trim)
    (map data-all-the-things)))

(defn apply-operation [registers op]
  (if ((:cond op) (get registers (:reg-cond op) 0) (:cond-pred op))
      (update registers (:reg-target op) (fnil (:operation op) 0) (:amount op))
      registers))

(defn run-instructions [parsed-instructions]
  (apply max (vals (reduce #(apply-operation %1 %2) {:a 0} parsed-instructions))))

(defn advent8-1 [^String instructions]
  (run-instructions (parse-8 instructions)))

(defn advent8-2 [instructions]
  (let [parsed-instructions (parse-8 instructions)
        count-instructions  (count parsed-instructions)
        take-fns            (map (comp take inc) (range count-instructions))
        instruction-trail   (map #(transduce % conj parsed-instructions) take-fns)]
    (apply max (map run-instructions instruction-trail))))
