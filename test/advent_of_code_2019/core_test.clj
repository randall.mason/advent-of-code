(ns advent-of-code-2019.core-test
  (:require [clojure.test :refer :all]
            [clojure.string :as s]
            [clojure.core.reducers :as r]
            [advent-of-code-2019.day10 :as day]))

#_(deftest advent01
   (testing "sum"
     (is (= 2 (day/answer1 "12")))
     (is (= 2 (day/answer1 "14")))
     (is (= 3302760 (day/answer1 (slurp "resources/2019/day01.txt")))))
   (testing "sum-sum"
     (is (= 2 (day/answer2 "14")))
     (is (= 966 (day/answer2 "1969")))
     (is (= 50346 (day/answer2 "100756")))
     (is (= 4951265 (day/answer2 (slurp "resources/2019/day01.txt"))))))

(def advent02-day-answer
  [4090701 12 2 2 1 1 2 3 1 3 4 3 1 5 0 3 2 13 1 60 1 19 10 64 2 10 23 256 1 27 6 258 1 13 31 263 1 13 35 268 1 39 10 272 2 43 13 1360 1 47 9 1363 2 51 13 6815 1 5 55 6816 2 59 9 20448 1 13 63 20453 2 13 67 102265 1 71 5 102266 2 75 13 511330 1 79 6 511332 1 83 5 511333 2 87 6 1022666 1 5 91 1022667 1 95 13 1022672 2 99 6 2045344 1 5 103 2045345 1 107 9 2045348 2 6 111 4090696 1 5 115 4090697 1 119 2 4090699 1 6 123 0 99 2 14 0 0])

#_(deftest advent02
   (testing "sum"
     (is (= [3500 9 10 70 2 3 11 0 99 30 40 50] (day/answer1 (day/parse-instructions "1,9,10,3,2,3,11,0,99,30,40,50") 0)))
     (is (= [2,0,0,0,99] (day/answer1 (day/parse-instructions "1,0,0,0,99") 0)))
     (is (= [2,3,0,6,99] (day/answer1 (day/parse-instructions "2,3,0,3,99") 0)))
     (is (= [2,4,4,5,99,9801] (day/answer1 (day/parse-instructions "2,4,4,5,99,0") 0)))
     (is (= [30,1,1,4,2,5,6,0,99] (day/answer1 (day/parse-instructions "1,1,1,4,99,5,6,0,99") 0)))
     (is (= advent02-day-answer (day/answer1 (day/alter-instruction (day/parse-instructions (slurp "resources/2019/day02.txt")) 12 2) 0))))
   (testing "sum-sum"
     (is (= 1202 (day/answer2 (day/parse-instructions (slurp "resources/2019/day02.txt")) 4090701)))
     (is (= 1202 (day/answer2 (day/parse-instructions (slurp "resources/2019/day02.txt")) 19690720)))))
  ;   (is (= 966 (day/answer2 "1969")))
  ;   (is (= 50346 (day/answer2 "100756")))
  ;   (is (= 4951265 (day/answer2 (slurp "resources/2019/day.txt"))))))

#_(deftest advent03
   #_(testing "one"
      (is (= 6 (day/answer1 "U7,R6,D4,L4\nR8,U5,L5,D3")))
      (is (= 159 (day/answer1 "R75,D30,R83,U83,L12,D49,R71,U7,L72\nU62,R66,U55,R34,D71,R55,D58,R83")))
      (is (= 135 (day/answer1 "R98,U47,R26,D63,R33,U87,L62,D20,R33,U53,R51\nU98,R91,D20,R16,D67,R40,U7,R15,U6,R7")))
      #_(is (= 248 (day/answer1 (slurp "resources/2019/day03.txt")))))
   (testing "two"
     (is (= 30 (day/answer2 "U7,R6,D4,L4\nR8,U5,L5,D3")))
     (is (= 610 (day/answer2 "R75,D30,R83,U83,L12,D49,R71,U7,L72\nU62,R66,U55,R34,D71,R55,D58,R83")))
     (is (= 410 (day/answer2 "R98,U47,R26,D63,R33,U87,L62,D20,R33,U53,R51\nU98,R91,D20,R16,D67,R40,U7,R15,U6,R7")))
     (is (= 28580 (day/answer2 (slurp "resources/2019/day03.txt"))))))

#_(deftest advent04
   (testing "one"
     (is (day/valid1 111111))
     (is (not (day/valid1 223450)))
     (is (not (day/valid1 123789)))
     #_(is (= 960 (day/answer1 [265275 781584]))))
   (testing "two"
     (is (day/valid2 112233))
     (is (not (day/valid2 123444)))
     (is (day/valid2 111122))
     #_(is (= 626 (day/answer2 [265275 781584])))))

#_(deftest advent05
   (testing "sum"
     (is (= [1002,4,3,4,99] (:program (day/answer (day/parse-instructions "1002,4,3,4,33") 0 nil ""))))
     (is (= [1101,100,-1,4,99] (:program (day/answer (day/parse-instructions "1101,100,-1,4,0") 0 nil ""))))
     (is (= [2,3,0,6,99] (:program (day/answer (day/parse-instructions "2,3,0,3,99") 0 nil ""))))
     (is (= [2,4,4,5,99,9801] (:program (day/answer (day/parse-instructions "2,4,4,5,99,0") 0 nil ""))))
     (is (= [30,1,1,4,2,5,6,0,99] (:program (day/answer (day/parse-instructions "1,1,1,4,99,5,6,0,99") 0 nil nil))))
     (is (= 11933517 (:computer-output (day/answer (day/parse-instructions (slurp "resources/2019/day05.txt")) 0 1 nil)))))
   (testing "sum-sum"
     #_(is (= 1202 (day/answer (day/parse-instructions (slurp "resources/2019/day05.txt")) 0 5 nil)))
     #_(is (= 1202 (day/answer (day/parse-instructions (slurp "resources/2019/day05.txt")) 0 1)))))

#_(deftest advent06
   (testing "indirect and direct orbits"
     (is (= 42 (day/answer1 "COM)B\nB)C\nC)D\nD)E\nE)F\nB)G\nG)H\nD)I\nE)J\nJ)K\nK)L")))
     (is (= 150150 (day/answer1 (slurp "resources/2019/day06.txt")))))
   (testing "indirect and direct orbits"
     (is (= 4 (day/answer2 "COM)B\nB)C\nC)D\nD)E\nE)F\nB)G\nG)H\nD)I\nE)J\nJ)K\nK)L\nK)YOU\nI)SAN")))
     (is (= 352 (day/answer2 (slurp "resources/2019/day06.txt"))))))

#_(deftest advent08
    (testing "indirect and direct orbits"
      (is (= 828 (day/answer1 (slurp "resources/2019/day08.txt")))))
    #_(testing "indirect and direct orbits"
       (is (= 352 (day/answer2 (slurp "resources/2019/day08.txt"))))))

(def example-map-1
  ".#..#\n.....\n#####\n....#\n...##")

(def count-los-map-1
  ".7..7\n.....\n67775\n....7\n...87")

(deftest advent10
  (testing "line of sight"
    (is (= (day/parse-asteroid-map count-los-map-1) (day/count-los example-map-1)))))
  
(comment
  (do
    (require '[clojure.tools.namespace.repl])
    (require '[clojure.test :refer [run-tests]] :reload-all)
    (require '[advent-of-code-2019.core-test])
    (do
      (clojure.tools.namespace.repl/refresh)
      (run-tests 'advent-of-code-2019.core-test))))
    
