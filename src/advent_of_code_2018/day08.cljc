(ns advent-of-code-2018.day08
  (:require [clojure.string :as s]))

(defn parse [tree-string]
  (mapv read-string (s/split (s/trim tree-string) #" ")))


(defn build-tree [tree-string level tree])
  

{:id 1
 :count-children 2
 :count-metadata 3
 :metadata [1 1 2]
 :children [{:id 2
             :metadata [10 11 12]
             :children []}
            {:id 3
             :metadata [2]
             :children [{:id 4
                         :children []
                         :metadata [99]}]}]}
