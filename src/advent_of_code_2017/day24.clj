(ns advent-of-code-2017.day24
  (:require [clojure.string :as s]
            [clojure.pprint :as pp]
            [clojure.walk :as w]))

(def not-nil?
  (complement nil?))

(defn bridge-starters [connectors]
  (filter (partial some #{0}) connectors))

(defn build-connectors [bridge-file]
  (map #(mapv bigint (s/split %1 #"/")) (s/split-lines bridge-file)))

(defn build-tree [bridge-head connectors]
  (let [candidates (filter not-nil? (map-indexed #(if (not= nil %2) (get (apply vector connectors) %1))
                                                  (mapv #(some #{(second (last bridge-head))} %) connectors)))
        remainder  (filter not-nil? (map-indexed #(if (= nil %2) (get (apply vector connectors) %1))
                                                  (mapv #(some #{(second (last bridge-head))} %) connectors)))
        candidates (mapv #(if (= (second (last bridge-head)) (first %)) % (reverse %)) candidates)]
   (if (some not-nil? (map #(some #{(second (last bridge-head))} %) connectors))
     [bridge-head (mapv #(build-tree (conj bridge-head %)
                                     (concat remainder (filter (partial not= %) candidates)))
                        candidates)]
     bridge-head)))


(defn build-bridges [bridge-file]
   (let [connectors (build-connectors bridge-file)
         starters   (bridge-starters connectors)
         bridges    (map (partial partition 2)
                         (map flatten (partition 2 (partition-by (partial = 0)
                                                       (flatten (mapv #(build-tree [%]
                                                                        (filter (partial not= %) connectors)) starters))))))]
      ;(pp/pprint bridges)
      bridges))

(defn strongest [bridges]
        (apply max (map (comp (partial reduce +) flatten) bridges)))

(defn strongest-longest [bridges]
  ;  (let [;flat-bridges (mapv flatten bridges)
  ;
  ;        bridge-map (into {} (map-indexed #(vector %1 ((comp count flatten) %2)) bridges))
  ;        max-bridge (val (apply max-key val (into {} (map-indexed #(vector %1 ((comp count flatten) %2)) bridges))))
  ;        longest-bridges (keys (filter #(= max-bridge (val %)) bridge-map))]
  ;     ;(println longest-bridges (mapv (partial into (vector)) bridges) bridge-map)
  ;     (apply max (map (comp (partial reduce +) flatten) (mapv #(get (mapv (partial into (vector)) bridges) %) longest-bridges)))))
    (let [sorted-bridges        (sort-by count bridges)
          longest-bridge-length (count (last sorted-bridges))
          longest-bridges       (take-while #(= longest-bridge-length (count %)) (reverse sorted-bridges))]
      (apply max (map (comp (partial reduce +) flatten) longest-bridges))))
