(ns advent-of-code-2018.core-test
  (:require [clojure.test :refer :all]
            [clojure.string :as s]
            [clojure.core.reducers :as r]
            ;; [advent-of-code-2018.day01 :as day01]
            ;; [advent-of-code-2018.day02 :as day02]
            ;; [advent-of-code-2018.day03 :as day03]
            ;; [advent-of-code-2018.day04 :as day04]
            ;; [advent-of-code-2018.day05 :as day05]
            ;; [advent-of-code-2018.day06 :as day06]
            ;; [advent-of-code-2018.day07 :as day07]))
            ;; [advent-of-code-2018.day08 :as day08]
            ;; [advent-of-code-2018.day09 :as day09]
            ;; [advent-of-code-2018.day10 :as day10]
            ;; [advent-of-code-2018.day11 :as day11]
            ;; [advent-of-code-2018.day12 :as day12]
            ;; [advent-of-code-2018.day13 :as day13]
            ;; [advent-of-code-2018.day14 :as day14]
            ;; [advent-of-code-2018.day15 :as day15]
            ;; [advent-of-code-2018.day16 :as day16]
            ;; [advent-of-code-2018.day17 :as day17]
            ;; [advent-of-code-2018.day18 :as day18]
            ;; [advent-of-code-2018.day19 :as day19]
            ;; [advent-of-code-2018.day20 :as day20]
            ;; [advent-of-code-2018.day21 :as day21]
            ;; [advent-of-code-2018.day25 :as day25]))

#_(deftest advent01
    (testing "sum"
      (is (= 493 (day01/sum-freqs (day01/parse-freqs (slurp "resources/2018/day01.txt"))))))
    (testing "reach-same"
      (is (= 0 (day01/repeat-freq? (day01/parse-freqs "+1\n-1"))))
      (is (= 10 (day01/repeat-freq? (day01/parse-freqs "+3\n+3\n+4\n-2\n-4"))))
      (is (= 0 (day01/repeat-freq? (day01/parse-freqs "+1\n-1"))))
      (is (= 14 (day01/repeat-freq? (day01/parse-freqs "+7\n+7\n-2\n-7\n-4"))))
      (is (= 5 (day01/repeat-freq? (day01/parse-freqs "-6\n+3\n+8\n+5\n-6"))))
      (is (= 413 (day01/repeat-freq? (day01/parse-freqs (slurp "resources/2018/day01.txt")))))))

#_(deftest advent02
    (testing "box-checksum"
      (is (= 12 (day02/checksum-boxids (day02/parse-boxids "abcdef\nbababc\nabbcde\nabcccd\naabcdd\nabcdee\nababab"))))
      (is (= 7688 (day02/checksum-boxids (day02/parse-boxids (slurp "resources/2018/day02.txt"))))))
    (testing "box-difference"
      (is (= "fgij" (day02/diff-similar (day02/find-similar (day02/parse-boxids "abcde
                                                                              fghij
                                                                              klmno
                                                                              pqrst
                                                                              fguij
                                                                              axcye
                                                                              wvxyz")))))
      (is (= "lsrivmotzbdxpkxnaqmuwcchj" (day02/diff-similar (day02/find-similar (day02/parse-boxids (slurp "resources/2018/day02.txt")))))))) 

#_(deftest advent03
    (testing "overlap"
      (is (= 4 (day03/find-overlap (day03/parse-claims "#1 @ 1,3: 4x4
                                 #2 @ 3,1: 4x4
                                 #3 @ 5,5: 2x2"))))
      (is (= 114946 (day03/find-overlap (day03/parse-claims (slurp "resources/2018/day03.txt"))))))
    (testing "non-overlap"
      (is (= 3 (day03/find-nonoverlap (day03/parse-claims "#1 @ 1,3: 4x4
                                                         #2 @ 3,1: 4x4
                                                         #3 @ 5,5: 2x2"))))
      (is (= 877 (day03/find-nonoverlap (day03/parse-claims (slurp "resources/2018/day03.txt")))))))

#_(deftest advent04
    (testing "longest-sleep"
       (is (= 10 (:guard (day04/most-hours-asleep (day04/parse-schedule (slurp "resources/2018/day04.test.txt"))))))
       (is (= 727 (:guard (day04/most-hours-asleep (day04/parse-schedule (slurp "resources/2018/day04.txt")))))))
    (testing "most sleepy time for longest sleeper"
       (is (= 240 (day04/most-asleep (day04/most-hours-asleep (day04/parse-schedule (slurp "resources/2018/day04.test.txt"))))))
       (is (= 21083 (day04/most-asleep (day04/most-hours-asleep (day04/parse-schedule (slurp "resources/2018/day04.txt")))))))
    (testing "regular-sleeper"
      (is (= 99 (key (:guard (day04/most-regular-sleeper (day04/parse-schedule (slurp "resources/2018/day04.test.txt")))))))
      (is (= 1657 (key (:guard (day04/most-regular-sleeper (day04/parse-schedule (slurp "resources/2018/day04.txt"))))))))
    (testing "regular-sleeper-minute"
      (let [guard-data (:guard (day04/most-regular-sleeper (day04/parse-schedule (slurp "resources/2018/day04.test.txt"))))]
        (is (= 4455 (* (key guard-data) (first (val guard-data)))))) 
      (let [guard-data (:guard (day04/most-regular-sleeper (day04/parse-schedule (slurp "resources/2018/day04.txt"))))]
        (is (= 53024 (* (key guard-data) (first (val guard-data))))))))

#_(deftest advent05.1
    (testing "reaction"
       (is (= 10      (count (day05/react [] (day05/parse (slurp "resources/2018/day05.test.txt"))))))
       (is (= 11310 (count (day05/react [] (day05/parse (slurp "resources/2018/day05.txt"))))))))
#_(deftest advent05.2
    (testing "elimination reaction"
       (is (= 4      (val (apply min-key val (day05/try-elim-elem (day05/parse (slurp "resources/2018/day05.test.txt")))))))
       (is (= 6020 (val (apply min-key val (day05/try-elim-elem (day05/parse (slurp "resources/2018/day05.txt")))))))))


#_(deftest advent06.1
    (testing "largest open area"
      #_(is (= 17 (day06/largest-open-area (slurp "resources/2018/day06.test.txt"))))
      #_(is (= 4060 (day06/largest-open-area (slurp "resources/2018/day06.txt"))))
      ; Watch until it stableizes  It's 7, 4060
      #_(do (use 'advent-of-code-2018.day06 :reload) (largest-open-area (slurp "resources/2018/day06.txt")))))
#_(deftest advent06.2
    (testing "distance to all points"
      (is (= 30    (day06/distance-to-all-points [4 3] (slurp "resources/2018/day06.test.txt"))))
      (is (= 20609 (day06/distance-to-all-points [4 3] (slurp "resources/2018/day06.txt"))))
      (is (= 16    (day06/distance-to-all-points-within 32 (slurp "resources/2018/day06.test.txt"))))
      (is (= 36136 (day06/distance-to-all-points-within 10000 (slurp "resources/2018/day06.txt"))))))

#_(deftest advent07.1
  (testing ""
    (is (= "CABDFE"  (:string (day07/solve (slurp "resources/2018/day07.test.txt") 1 0))))
    (is (= "ABDCJLFMNVQWHIRKTEUXOZSYPG" (:string (day07/solve (slurp "resources/2018/day07.txt") 1 0))))))
#_(deftest advent07.2
  (testing ""
    (is (= {:string "CABFDE" :timer 15} (day07/solve (slurp "resources/2018/day07.test.txt") 2 0)))
    (is (= {:string "ABLDNFWMCJVRHQITXKEUZOSYPG" :timer 896} (day07/solve (slurp "resources/2018/day07.txt") 5 60)))))

(comment
    (require '[clojure.test :refer [run-tests]])
    (require 'advent-of-code-2018.core-test)
    (run-tests 'advent-of-code-2018.core-test))
