(ns advent-of-code-2019.day04
  (:require [clojure.string :as s]))

(defn valid1 [password] 
  (and
   (< (count (partition-by identity (str password))) 6)
   (every? (partial >= 0) (map #(apply - %) (partition 2 1 (map #(Character/getNumericValue %) (str password)))))))

(defn valid2 [password]
  (and
   (some (partial = 2) (map count (partition-by identity (str password))))
   (every? (partial >= 0) (map #(apply - %) (partition 2 1 (map #(Character/getNumericValue %) (str password)))))))

(defn answer1 [input]
  (count (filter valid1 (range (first input) (inc (second input))))))
    
(defn answer2 [input]
  (count (filter valid2 (range (first input) (inc (second input))))))
