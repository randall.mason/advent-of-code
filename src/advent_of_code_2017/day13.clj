(ns advent-of-code-2017.day13
  (:require [clojure.string :as s]
            [clojure.pprint :as pp]))

(defn advent13-1 [input]
  (println input)
  42)

(defn load-firewall [firewall-desc]
  (into {} (map #(s/split % #": ") (s/split-lines firewall-desc))))
