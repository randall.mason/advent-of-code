(ns advent-of-code-2019.day05
  (:require [clojure.string :as s]))

(defn parse-instructions [program]
  (into [] (map read-string (s/split program #","))))

(defmacro header [op]
  `(let [~'program (~'computer :program)
         ~'instruction-position (~'computer :instruction-position)
         ~'instruction (parse-instruction (get ~'program ~'instruction-position))
         ~'modes (parse-mode ~'instruction)
         ~'opcode (parse-opcode ~'instruction)
         ~'input (~'computer :input)
         ~'computer-output (~'computer :computer-output)
         [~'instruction-fn ~'input-number ~'output-number] (instructions ~'opcode)
         ~'inputs (take ~'input-number (drop (inc ~'instruction-position) ~'program))
         ~'outputs (take ~'output-number (drop (+ 1 ~'input-number ~'instruction-position) ~'program))]
     ~op))

(defn addr-mode-to-addr [instruction-position modes input-number output-number]
  (for [mode modes
        :let [y (* mode 3)]
        :when (even? y)]
      y))
  
(def instructions
  {1 [+ 2 1]
   2 [* 2 1]
   3 [assoc 0 1]
   4 [identity 1 0]
   5 [identity 2 0]
   6 [identity 2 0]
   7 [assoc 2 0]
   8 [assoc 2 0]
   99 [:halt 0 0]})

(defn opcode-fn [opcode]
  (first (instructions opcode)))

; "0" is position mode  - ADDRESS
; "1" is immediate mode - USE THAT VALUE

(defn mode-fn [mode]
  (if (= "0" mode)
    get
    (fn [x y] y)))

(defn parse-opcode [instruction]
  (read-string (clojure.string/join (drop 3 instruction))))

(defn parse-instruction [inst]
  (s/split (format "%05d" inst) #""))

(defn parse-mode [instruction]
  (reverse (map mode-fn (take 3 instruction))))

(defn next-instruction-position [computer]
  (header
    (reduce + (inc instruction-position) (rest (instructions (parse-opcode (parse-instruction (get program instruction-position))))))))

(defn op-output [computer]
  (header
   (assoc computer
      :computer-output (instruction-fn ((first modes) program (first (take input-number (drop (inc instruction-position) program)))))
      :instruction-position (next-instruction-position computer))))

(defn op-input [computer]
  (header
   (assoc computer
      :program (assoc program (get program (+ instruction-position input-number 1)) input)
      :instruction-position (next-instruction-position computer))))

(defn op-jump-if-true [computer]
  (header
   (if (= 0 (first inputs))
     computer
     (assoc computer
       :instruction-position (second inputs)))))

(defn op-jump-if-false [computer]
  (header
   (if (not= 0 (first inputs))
     computer
     (assoc computer
       :instruction-position (second inputs)))))

(defn op-store-if-less [computer]
  (header
   (if (not= 0 (first inputs))
     computer
     (assoc computer
       :instruction-position (second inputs)))))

(defn op-store-if-equals [computer]
  (header
   (if (not= 0 (first inputs))
     computer
     (assoc computer
       :instruction-position (second inputs)))))

(defn generic-opcode [computer]
  (header
   (assoc program ((last modes) program (+ instruction-position input-number 1))
     (apply
       instruction-fn
       (map
         #(get program ((nth modes %) program (+ instruction-position 1 %)))
         (range input-number))))))

(defn execute-instruction [computer]
  (header
   (if (= 99 opcode)
      computer
      (recur
       (case opcode
         8 (op-store-if-equals computer)
         7 (op-store-if-less computer)
         6 (op-jump-if-false computer)
         5 (op-jump-if-true computer)
         4 (op-output computer)
         3 (op-input computer)
        {:program (generic-opcode computer)
         :instruction-position (next-instruction-position computer)
         :input input
         :computer-output computer-output})))))


(defn answer [program instruction-position input computer-output]
    (execute-instruction {:program program
                          :instruction-position instruction-position
                          :relative-offset 0
                          :input input
                          :computer-output computer-output}))

(comment
  (do
    (require '[clojure.tools.namespace.repl])
    (require '[clojure.test :refer [run-tests]] :reload-all)
    (require '[advent-of-code-2019.core-test])
    (do
      (clojure.tools.namespace.repl/refresh)
      (run-tests 'advent-of-code-2019.core-test)))
  (def program [1002,4,3,4,33])
  (def instruction-position 0)
  (def input-number 2)
  (def modes [get (fn [x y] y) get])
  (nth modes 0))
