(ns advent-of-code-2018.day05
  (:require [clojure.string :as s]
            [clojure.core.reducers :as r]
            [criterium.core :as cr]))

(defn parse [string]
  (-> string
      s/trim
      seq))

; snagged from mfikes
(defn reacts? [x y]
  (and (not= x y)
       (= (s/lower-case x) (s/lower-case y))))

(defn check-letter
  ([] [])
  ([elim]
   [])
  ([result current-letter]
   (cond
     (= 0 (count result)) [current-letter]
     (reacts? (peek result) current-letter) (pop result)
     :else (conj result current-letter)))
  ([elim result current-letter]
   (cond
     (= elim (s/lower-case current-letter)) result
     :else (check-letter result current-letter))))

(defn react
  ([checked polymer]
   (r/fold 15000 check-letter check-letter polymer))
  ([checked polymer elim]
   (r/fold 15000 (partial check-letter elim) (partial check-letter elim) polymer)))

(defn find-elements [polymer]
 (into #{} (map s/lower-case (into #{} polymer))))

(defn try-elim-elem [polymer]
 (into {} (for [elem (find-elements polymer)]
             {elem (count (react [] polymer elem))})))
