(ns advent-of-code-2017.day01
  (:require [clojure.string :as s]
            [clojure.pprint :as pp]
            [clojure.walk :as w]))
(defn interleaving [captcha]
  (interleave (take (count captcha) (cycle captcha)) (take (count captcha) (drop (/ (count captcha) 2) (cycle captcha)))))

(defn get-runs [take-num-fn part-fn part-by-fn captcha]
  (apply vector (part-fn part-by-fn (take (take-num-fn captcha) (cycle captcha)))))

(defn find-sufficient-runs [length captcha]
  (filter (fn [x] (< length (count x))) captcha))

(defn char-to-int [char]
  (- (int char) 48))

(defn string-to-int [string]
  (biginteger ^String string))

(defn advent1-1 [captcha]
  (->> captcha
   (get-runs #(inc (count %)) partition-by identity)
   (find-sufficient-runs 1)
   (map rest)
   flatten
   (map int)
   (map #(- % 48))
   (reduce +)))

(defn advent1-2 [captcha]
  (->> captcha
   interleaving
   (get-runs count partition 2)
   (find-sufficient-runs 1)
   (filter #(= (first %) (last %)))
   (map rest)
   flatten
   (map int)
   (map #(- % 48))
   (reduce +)))
