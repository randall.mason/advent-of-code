(ns advent-of-code-2017.day09
  (:require [clojure.string :as s]
            [clojure.pprint :as pp]
            [clojure.walk :as w]))


(defn garbage-deletion [stream-string]
  (-> stream-string
    (s/replace #"!." "")
    ; (#(do (println (count %))
    ;       %))
    (s/replace #"<[^>]*>" "")))
    ; (#(do (println (count %))
    ;       %))))

(defn garbage-deletion-with-count [stream-string]
  (let [remove-canceled (s/replace stream-string #"!." "")
        remove-inside-garbage (s/replace remove-canceled #"<[^>]*>" "<>")]
    {:cleaned-string (s/replace remove-inside-garbage #"<[^>]*>" "")
     :garbage-deleted (- (count remove-canceled) (count remove-inside-garbage))}))
(defn count-groups [stream-string]
  (count (s/replace stream-string #"[^{]" "")))

(defn score
  ([string]
   (score string 0 0))
  ([string depth score]
   (case (first string)
       nil  score
       \{  (recur (rest string) (inc depth) (+ score (inc depth)))
       \}  (recur (rest string) (dec depth) score)
       (recur (rest string) depth score))))
