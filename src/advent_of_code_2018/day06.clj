(ns advent-of-code-2018.day06
  (:require [clojure.string :as s]
            [clojure.core.reducers :as r]
            [criterium.core :as cr]))

(defn char-range
 ([start end]
  (map char (range (int start) (inc (int end)))))
 ([start]
  (map char (range (int start) Double/POSITIVE_INFINITY))))

(defn parse [string]
  (->> string
       s/split-lines
       (map #(s/split %1 #", "))
       (map (partial map read-string))
       (map (fn [id cord]
              (into {}
                    (map #(hash-map %1 %2)
                         [:id :x :y]
                         (conj cord id))))
            (char-range \0))
       (map #(hash-map  [(:x %) (:y %)] {:id (:id %) :distance 0}))
       (apply merge)))

(defn candidates [points-vals]
  (->> points-vals
      (filter #(and (not= ((comp first  first) (apply min-key (comp first  first) points-vals))
                          ((comp first  first) %))
                    (not= ((comp first  first) (apply max-key (comp first  first) points-vals))
                          ((comp first  first) %))
                    (not= ((comp second first) (apply min-key (comp second first) points-vals))
                          ((comp second first) %))
                    (not= ((comp second first) (apply max-key (comp second first) points-vals))
                          ((comp second first) %))))
      (map #(assoc-in % [1 :candidate] true))
      (into {})
      (merge points-vals)))

(defn one-manhattan [[x y]]
  [[(inc x) y]
   [x (inc y)]
   [(dec x) y]
   [x (dec y)]])

(defn is-already-in-points? [points new-point]
  (contains? points new-point))

(defn disown-if-equal-distance [old-point points new-point]
  (if (and (= (inc ((comp :distance second) old-point)) (get-in points [new-point :distance]))
           (not= (get-in points [new-point :id]) ((comp :id second) old-point)))
      (assoc-in points [new-point :id] ".")
      points))

(defn add-if-not-owned [old-point points new-point]
  (assoc points new-point (update (second old-point) :distance inc)))

(defn grow-point [points point]
  (let [new-points (one-manhattan (first point))
        points     (reduce (partial disown-if-equal-distance point) points
                      (filter (partial is-already-in-points? points) new-points))]
     (reduce (partial add-if-not-owned point) points
       (filter (complement (partial is-already-in-points? points)) new-points))))

(defn grow-points [points distance]
  (->> points
       (filter #(and (= distance ((comp :distance second) %))
                     (not= "."   ((comp :id second) %))))
       (reduce grow-point points)))

(defn candidate-sum [points]
  (count (filter #((comp :candidate second) %) points)))


(defn watch-progress [points]
  (->> points
       (filter (comp :candidate second))
       (group-by (comp :id second))
       (map #(hash-map (first %) (count (second %))))
       (into {})
       (sort-by val)))

(defn get-growth [points]
  (->> points
       (filter (comp :candidate second))
       (group-by (comp :id second))
       (apply max-key (comp count val))
       val
       count))

(defn largest-open-area [points-string]
   (loop [depth   0
          points  (-> points-string
                      parse
                      candidates)
          total-candidate (candidate-sum points)]
     (println (watch-progress points))
     (let [new-points (grow-points points depth)
           new-total-candidates (candidate-sum new-points)]
       (if (< total-candidate new-total-candidates)
         (recur (inc depth) new-points new-total-candidates)
         (get-growth new-points)))))

(defn distance-to-points [[x y] points]
  (reduce +
          (map #(+ (Math/abs (- x (first %)))
                   (Math/abs (- y (second %)))) points)))

(defn distance-to-all-points [coord points-string]
  (let [points (->> points-string
                    parse
                    (map first))]
    (distance-to-points coord points)))

(defn distance-to-all-points-within [distance points-string]
  (let [points (->> points-string
                    parse
                    (map first))
        max-x (first (apply max-key first points))
        max-y (second (apply max-key second points))
        min-x (first (apply min-key first points))
        min-y (second (apply min-key second points))]
    (count (filter (comp (partial > distance) second) (for [x (range min-x max-x) 
                                                            y (range min-y max-y)]
                                                        [[x y] (distance-to-points [x y] points)])))))
