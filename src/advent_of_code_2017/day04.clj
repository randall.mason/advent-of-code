(ns advent-of-code-2017.day04
  (:require [clojure.string :as s]
            [clojure.pprint :as pp]
            [clojure.walk :as w]))

(defn char-to-int [char]
  (- (int char) 48))

(defn string-to-int [string]
  (biginteger ^String string))

(defn split-words [string]
  (s/split string #" "))

(defn advent4-1 [pass]
  (= (-> pass
      split-words
      set
      count)
     (count (s/split pass #" "))))

(defn uniq-anagrams [pass]
  (->> pass
    split-words
    (map sort)
    set
    count))

(defn advent4-2 [pass]
  (= (uniq-anagrams pass)
     (count (s/split pass #" "))))
