(ns advent-of-code-2019.day03
  (:require [clojure.string :as s]
            [clojure.set    :as set-tools]))

(def directions
  {:U [0 1]
   :D [0 -1]
   :L [-1 0]
   :R [1 0]})

(defn parse-wire-segment [seg]
  (let [delta (directions (keyword (str (first seg))))
        length (read-string (s/join "" (rest seg)))]
   {:delta delta
    :length length}))

(defn add-points [list-of-points new-segment]
  (if (= 0 (:length new-segment))
    list-of-points
    (let [start-point (last list-of-points)
          new-point   (into [] (map + start-point (:delta new-segment)))]
      (recur (conj list-of-points new-point) (update new-segment :length dec)))))

(defn extrude [wire]
  (reduce add-points [[0 0]] wire))

(defn parse-wire [wire]
  (-> wire
      (s/split #",")
      (#(map parse-wire-segment %))
      extrude
      (#(into [] %))))

(defn parse-instructions [program]
  (into [] (map parse-wire (s/split-lines program))))

(def manhatan-distance
  (comp (partial apply +) (partial map #(Math/abs ^long %))))

(defn find-intersections [wires]
  (set-tools/difference (apply set-tools/intersection
                                               (map set wires))
                                        #{[0 0]}))

(defn positions-of-intersections [intersections wire]
  (into [] (keep-indexed (fn [idx val] (when (intersections val) {val idx})) wire)))

(defn answer1 [input]
  (apply min (map manhatan-distance (find-intersections (parse-instructions input)))))

(defn answer2 [input]
  (let [wires (parse-instructions input)
        intersections (find-intersections wires)]
    (second (apply min-key second (apply merge-with + (map (comp (partial into {}) (partial positions-of-intersections intersections)) wires))))))