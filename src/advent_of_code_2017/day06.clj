(ns advent-of-code-2017.day06
  (:require [clojure.string :as s]
            [clojure.pprint :as pp]
            [clojure.walk :as w]))

(defn char-to-int [char]
  (- (int char) 48))

(defn string-to-int [string]
  (biginteger ^String string))

(defn next-loc [location length]
  (if (< (inc location) length)
      (inc location)
      0))

(defn distribute-largest [memory-banks next mem]
  (if (= 0 mem)
      memory-banks
      (recur (update-in memory-banks [next] inc)
             (next-loc next (count memory-banks))
             (dec mem))))

(defn find-largest [memory-banks]
  (let [max-mem (apply max memory-banks)
        max-mem-loc (.indexOf memory-banks max-mem)
        next (next-loc max-mem-loc (count memory-banks))]
      (distribute-largest
        (assoc memory-banks max-mem-loc 0N)
        next
        max-mem)))


(defn advent6-1
 ([memory-banks]
  (-> memory-banks
   s/trim
   (s/split #"\t")
   ((partial map bigint))
   ((partial apply vector))
   (#(advent6-1 % 0 []))))
 ([memory-banks steps orig]
  ;(println memory-banks steps orig)
  (if (and (< 0 steps) (< 0 (.indexOf orig memory-banks)))
      [steps (- steps (.indexOf orig memory-banks))]
      (let [next (find-largest memory-banks)]
        (recur next (inc steps) (conj orig memory-banks))))))

#_(advent6-1 "0\t2\t7\t0")
