(ns advent-of-code-2017.day14
  (:require [clojure.string :as s]
            [clojure.pprint :as pp]
            [advent-of-code-2017.day10 :as day10]))

; from https://stackoverflow.com/q/10062967
(defn hex-string-to-byte-array [hex-string]
    (map
      (fn [[x y]] (Integer/parseInt (str x y) 16))
      (partition 1 hex-string)))

(defn bytes-to-binary-string [bytes]
  (apply str (map (partial pp/cl-format nil "~8,'0',B") bytes)))

(defn make-grid [in-key]
  (map #(s/split (bytes-to-binary-string %) #"")
        (map (partial day10/multi-round-knot-hash 64)
             (map (partial str in-key)
                  (range 128)))))

(defn count-bytes [disk-image]
  (reduce +
    (map count
        (map #(filter (partial = "1") %) disk-image))))

(defn replace-all [disk-image old new]
  (mapv (fn [row] (mapv (fn [el] (if (= old el) new el)) row)) disk-image))

(defn get-adj-region [disk-image row col]
  (cond
    (= 0 row col)  nil))

(defn inc-2d [[row col] col-max]
  (cond
    (= col (dec col-max)) [(inc row) 0]
    :else                 [row (inc col)]))

(defn count-contiguous
  ([disk-image]
   ;(mapv println disk-image)
   (dec (count (set (flatten (count-contiguous (replace-all (replace-all disk-image "0" 0) "1" 1) [0 1] (count (first disk-image)) 2))))))
  ([disk-image [row col] row-length next-region]
   (let [previous-cell (get-in disk-image [row       (dec col)])
         higher-cell   (get-in disk-image [(dec row) col])
         current-cell  (get-in disk-image [row       col])]
    ;(println row col next-region current-cell previous-cell higher-cell)
    (cond
       (= 0 current-cell) (recur disk-image (inc-2d [row col] row-length) row-length next-region)
       (and
         (or (= 0 higher-cell) (nil? higher-cell))
         (and (not (nil? previous-cell)) (< 0 previous-cell))
         (= 1 current-cell))
       (recur (assoc-in disk-image [row col] previous-cell)
              (inc-2d [row col] row-length)
              row-length next-region)
       (and
         (or (= 0 higher-cell) (nil? higher-cell))
         (= 0 previous-cell)
         (= 1 current-cell))
       (recur (assoc-in disk-image [row col] next-region)
              (inc-2d [row col] row-length)
              row-length (inc next-region))
       (and
         (nil? previous-cell)
         (= 0 higher-cell)
         (= 1 current-cell))
       (recur (assoc-in disk-image [row col] next-region)
              (inc-2d [row col] row-length)
              row-length (inc next-region))
       (and
         (< 0 higher-cell)
         (and (not (nil? previous-cell)) (< 0 previous-cell))
         (= 1 current-cell))
       (recur (replace-all (assoc-in disk-image [row col] previous-cell) higher-cell previous-cell)
              (inc-2d [row col] row-length)
              row-length next-region)
       (and
         (< 0 higher-cell)
         (or (= 0 previous-cell) (nil? previous-cell))
         (= 1 current-cell))
       (recur (assoc-in disk-image [row col] higher-cell)
              (inc-2d [row col] row-length)
              row-length next-region)
       (nil? current-cell) disk-image))))

;       0       (recur disk-image row (inc col) next-region)))))
;       "1"     (recur (assoc-in disk-image [row col] next-region) row (inc col) (inc next-region))
;       1       (recur (assoc-in disk-image [row col] next-region) row (inc col) (inc next-region))
;       :else   (println "else")
;       number? (recur disk-image row (inc col) next-region)))))
