(ns advent-of-code-2015.day02
  (:require [clojure.string :as s]
            [clojure.pprint :as pp]
            [clojure.math.combinatorics :as combo]))

(defn parse-box [box-string]
  (map bigint (rest (re-matches #"([0-9]+)x([0-9]+)x([0-9]+)" box-string))))

(defn import-boxes [boxes]
  (s/split-lines (s/trim (slurp boxes))))

(defn calculate-required-area [box]
  (let [combinations  (combo/combinations '(1 2 3) 2)
        parsed-box  (into [] (parse-box box))
        applied-boxes (mapv (partial mapv parsed-box) (combo/combinations '(0 1 2) 2))
        areas (->> applied-boxes
                (mapv #(reduce * %)))]
   (reduce + (conj (mapv (partial * 2) areas) (apply min areas)))))

(defn calculate-ribon-length [box]
  (* 2 (reduce + (take 2 (sort (into [] (parse-box box)))))))

(defn calculate-bow-length [box]
  (reduce * (into [] (parse-box box))))

(defn calculate-total-length [box]
  (+ (calculate-ribon-length box) (calculate-bow-length box)))
