(ns advent-of-code-2017.day12
  (:require [clojure.string :as s]
            [clojure.pprint :as pp]
            [clojure.walk :as w]
            [loom.graph :as graph]
            [loom.io :as io]
            [loom.alg :as alg]
            [loom.derived :as derived]))

(defn load-pipes [pipe-string]
  (->> pipe-string
    s/split-lines
    (map s/trim)
    (into {} (map-indexed #(vector (bigint %1) (map bigint (s/split (nth (re-find #"^(.*) <-> (.*)$" %2) 2) #", ")))))))

(defn advent12-1 [pipes]
  (let [g (graph/graph pipes)]
    (count (graph/nodes (derived/subgraph-reachable-from g 0)))))

(defn advent12-2 [pipes]
  (let [g (graph/graph pipes)]
    (count (alg/connected-components g))))
