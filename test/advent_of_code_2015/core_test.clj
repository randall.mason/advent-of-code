(ns advent-of-code-2015.core-test
  #_(:require [clojure.test :refer :all]
              [clojure.string :as s]
              [clojure.core.reducers :as r]
              [advent-of-code-2015.day01 :as day01]
              [advent-of-code-2015.day02 :as day02]
              [advent-of-code-2015.day03 :as day03]
              [advent-of-code-2015.day04 :as day04]
              [advent-of-code-2015.day05 :as day05]
              [advent-of-code-2015.day06 :as day06]
              [advent-of-code-2015.day07 :as day07]
              [advent-of-code-2015.day08 :as day08]
              [advent-of-code-2015.day09 :as day09]
              [advent-of-code-2015.day10 :as day10]
              [advent-of-code-2015.day11 :as day11]
              [advent-of-code-2015.day12 :as day12]
              [advent-of-code-2015.day13 :as day13]
              [advent-of-code-2015.day14 :as day14]
              [advent-of-code-2015.day15 :as day15]
              [advent-of-code-2015.day16 :as day16]
              [advent-of-code-2015.day17 :as day17]
              [advent-of-code-2015.day18 :as day18]
              [advent-of-code-2015.day19 :as day19]
              [advent-of-code-2015.day20 :as day20]
              [advent-of-code-2015.day21 :as day21]
              [advent-of-code-2015.day25 :as day25]))

#_(deftest advent01
    (testing "level of imbalance"
      (is (= #{0} (set [(day01/imbalance "(())") 
                        (day01/imbalance "()()")])))

      (is (= 138 (day01/imbalance (slurp "resources/2015/advent01.txt")))))
    (testing "first position you enter the basement"
      (is (= 1 (day01/enter-basement ")")))
      (is (= 5 (day01/enter-basement "()())")))
      (is (= 1771 (day01/enter-basement (slurp "resources/2015/advent01.txt"))))))

#_(deftest advent02
    (testing "paper requirements"
      (is (= 58 (day02/calculate-required-area "2x3x4")))
      (is (= 43 (day02/calculate-required-area "1x1x10"))))
    (testing "multiple boxes"
      (is (= 101 (reduce + (map day02/calculate-required-area (s/split-lines "2x3x4\n1x1x10")))))
      #_(is (= 1586300 (reduce + (map day02/calculate-required-area (day02/import-boxes "resources/2015/advent02.txt"))))))
    (testing "ribbon length"
      (is (= 34 (day02/calculate-total-length "2x3x4")))
      (is (= 14 (day02/calculate-total-length "1x1x10")))
      (is (= 48 (reduce + (map day02/calculate-total-length (s/split-lines "2x3x4\n1x1x10")))))
      #_(is (= 483737498 (reduce + (map day02/calculate-total-length (day02/import-boxes "resources/2015/advent02.txt")))))))

#_(deftest advent03
    (testing "directions"
      #_(is (= 2 (day03/count-houses (day03/visit-houses ">"    [0 0]))))
      #_(is (= 4 (day03/count-houses (day03/visit-houses "^>v<" [0 0]))))))
