(ns advent-of-code-2019.day06
 (:require [clojure.string :as s]
           [loom.graph :as graph]
           [loom.io :as io]
           [loom.alg :as alg]
           [loom.derived :as derived]))


(defn parse-orbits [masses]
  (mapv #(s/split % #"\)") (s/split-lines masses)))

(defn answer1 [input]
  (->> input
       parse-orbits
       (apply graph/weighted-digraph)
       (#(alg/dijkstra-traverse % "COM"))
       last
       second
       seq
       flatten
       (filter number?)
       (reduce +)))

(defn answer2 [input]
  (->> input
       parse-orbits
       (apply graph/weighted-graph)
       (#(alg/dijkstra-path-dist % "YOU" "SAN"))
       second
       (+ -2)))
