(ns advent-of-code-2018.day03
  (:require [clojure.string :as s]
            [clojure.set    :as st]))

(defn parse-claims [boxid-strings]
  (->> boxid-strings
      s/split-lines
      (map s/trim)
      (map (partial re-matches  #"#(\d*) @ (\d+),(\d+): (\d+)x(\d+)"))
      (map #(flatten (list (first %) (map read-string (rest %)))))
      (map (fn [claim]
             (into {}
                   (map #(hash-map %1 %2)
                        [:orig :claim-id :start-x :start-y :length-x :length-y]
                        claim))))))

(defn claim-to-coords [claim]
  (into {} (for [y (range (:start-y claim) (+ (:length-y claim) (:start-y claim)))
                 x (range (:start-x claim) (+ (:length-x claim) (:start-x claim)))]
             {[x y] [(:claim-id claim)]})))

(defn merge-claims [claims]
  (apply merge-with into (map claim-to-coords claims)))

(defn find-overlap [claims]
  (count (filter (comp #(< 1 %) count) (vals (merge-claims claims)))))

(defn find-nonoverlap [claims]
  (first (st/difference (into #{} (map :claim-id claims))
                        (into #{} (flatten (filter (comp #(< 1 %) count) (vals (merge-claims claims))))))))
