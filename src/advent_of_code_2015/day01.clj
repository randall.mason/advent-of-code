(ns advent-of-code-2015.day01
  (:require [clojure.string :as s]
            [clojure.pprint :as pp]))

(defn parse [paren-string]
  (map #(if (= % "(") 1 -1) (s/split paren-string #"")))

(defn imbalance [paren-string]
  (reduce + (parse paren-string)))

(defn list-of-steps [paren-string]
  (map #(take % (parse paren-string)) (range 1 (inc (count paren-string)))))

(defn enter-basement [paren-string]
  (inc (.indexOf (mapv (partial reduce +) (list-of-steps paren-string)) -1)))
