(ns advent-of-code-2018.day01
  (:require [clojure.string :as s]))

(defn parse-freqs [freqs]
  (map read-string (s/split-lines freqs)))

(defn sum-freqs [freqs]
  (reduce + freqs))

(defn reductio-ad-absurdum [freqs]
   (reductions #(if (contains? %1 %2)
                  (reduced %2)
                  (conj %1 %2))
               #{0}
               (reductions + (cycle freqs))))

(defn repeat-freq? [freqs]
  (first (drop-while (comp not number?) (reductio-ad-absurdum freqs))))
