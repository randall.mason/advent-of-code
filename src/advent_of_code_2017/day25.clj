(ns advent-of-code-2017.day25
  (:require [clojure.string :as s]
            [clojure.pprint :as pp]))

(def machine (atom {}))

(def dir-to-value
  {:right 1
   :left -1})

(defn parse-state [machine lines-of-instructions]
  (let [state-name (keyword (second (re-find #"In state ([a-zA-Z])" (first lines-of-instructions))))]
    (swap! machine assoc-in [state-name (bigint (second (re-find #"current value is ([0-9]+)" (nth lines-of-instructions 1))))]
                            {:write (bigint (second (re-find #"Write the value ([0-9]+)" (nth lines-of-instructions 2))))
                             :move (dir-to-value (keyword (second (re-find #"Move one slot to the ([a-z]+)" (nth lines-of-instructions 3)))))
                             :next-state (keyword (second (re-find #"Continue with state ([a-zA-Z]+)" (nth lines-of-instructions 4))))})
    (swap! machine assoc-in [state-name (bigint (second (re-find #"current value is ([0-9]+)" (nth lines-of-instructions 5))))]
                            {:write (bigint (second (re-find #"Write the value ([0-9]+)" (nth lines-of-instructions 6))))
                             :move (dir-to-value (keyword (second (re-find #"Move one slot to the ([a-z]+)" (nth lines-of-instructions 7)))))
                             :next-state (keyword (second (re-find #"Continue with state ([a-zA-Z]+)" (nth lines-of-instructions 8))))})))

(defn parse-computer [etched-tablet]
  (let [etched-lines (s/split-lines etched-tablet)]
    (swap! machine assoc :current-step 0)
    (swap! machine assoc :cursor 1)
    (swap! machine assoc :step 0)
    (swap! machine assoc :tape (into [] (take 2 (repeat 0))))
    (swap! machine assoc :current-state (keyword (second (re-find #"Begin in state ([a-zA-Z])" (first etched-lines)))))
    (swap! machine assoc :check-after (bigint (second (re-find #"after ([0-9]+) steps" (second etched-lines)))))
    (mapv (partial parse-state machine) (partition 9 10 (drop 3 etched-lines)))
    machine))

(defn run-computer
  [machine-code]
  ;(pp/pprint @machine-code)
  (if (= (get @machine-code :step) (get @machine-code :check-after))
      machine-code
      (let [next-instruction
            (get-in @machine [(:current-state @machine-code)
                              (get-in @machine-code [:tape (:cursor @machine)])])]
        ;(println next-instruction)
        (swap! machine assoc-in
          [:tape (:cursor @machine)] (:write next-instruction))
        (swap! machine assoc-in
          [:current-state] (:next-state next-instruction))
        (swap! machine assoc-in
          [:cursor] (+ (:cursor @machine) (:move next-instruction)))
        (if (not (< 0 (:cursor @machine) (dec (count (:tape @machine-code)))))
            (if (= 0 (:cursor @machine))
                (do
                  (swap! machine update-in [:tape] (partial into [0]))
                  (swap! machine update-in [:cursor] inc))
                (do
                  (swap! machine update-in [:tape] #(conj % 0)))))
        ;(pp/pprint machine)
        (swap! machine update-in [:step] inc)
        (recur machine-code))))

(defn diagnostic-checksum [computer-output]
  (reduce + (:tape @computer-output)))
