(ns advent-of-code-2017.day07
  (:require [clojure.string :as s]
            [clojure.pprint :as pp]
            [clojure.walk :as w]))

(defn parse-element [element-string]
  (let [[root, children] (s/split element-string #" -> ")
        children         (if children (s/split children #"\, "))
        [_ name weight]        (re-find #"^(.*) \((.*)\)$" root)]
    {:process name :weight (bigint weight) :children children}))

(defn build-tree-from-roots-leaves [roots leaves]
 (if ((comp (partial = 0) count) roots)
     (first leaves)
  (let [leaf-id (:process (first leaves))
        root-hash (first (filter #(<= 0 (.indexOf (:children %) leaf-id)) roots))
        root-id   (.indexOf roots root-hash)
        child-id  (.indexOf (:children root-hash) leaf-id)]
    (if (= 1 (count (filter string? (:children root-hash))))
     (recur (into [] (concat (subvec roots 0 root-id)
                             (subvec roots (inc root-id))))
            (conj (rest leaves)
                  (assoc-in root-hash [:children child-id] (first leaves))))
     (recur (assoc-in roots [root-id :children child-id] (first leaves)) (rest leaves))))))


(defn advent7-1 [process-list]
  (let [elements (->> process-list
                   s/split-lines
                   (map s/trim)
                   (map parse-element))
        leaves (apply vector (filter #(nil? (:children %)) elements))
        roots   (apply vector (filter #(:children %) elements))]
      (:process (build-tree-from-roots-leaves roots leaves))))

(defn sum-children [element]
  ;(pp/pprint element)
  ;(if (nil? (:children element))
  ;    (pp/pprint (assoc element :child-weight (:weight element))))
  (if (nil? (:children element))
      (assoc element :total-weight (:weight element)
                     :child-weight 0)
      (assoc element :children     (map sum-children (:children element))
                     :child-weight (reduce + (pmap (comp :total-weight sum-children) (:children element)))
                     :total-weight (+ (:weight element) (reduce + (pmap (comp :total-weight sum-children) (:children element))))
                     :child-weights (pmap (comp :total-weight sum-children) (:children element)))))
(defn build-tree [process-list]
  (let [elements (->> process-list
                   s/split-lines
                   (map s/trim)
                   (map parse-element))
        leaves   (apply vector (filter #(nil? (:children %)) elements))
        roots    (apply vector (filter #(:children %) elements))]
    (build-tree-from-roots-leaves roots leaves)))

(defn walk-tree [process-tree]
  (let [child-weights      (map :total-weight  (:children process-tree))
        grandchild-weights (map :child-weights (:children process-tree))
        good-weights       (first (first (filter #(< 1 (val %)) (frequencies child-weights))))
        bad-weight        (first (first (filter #(= 1 (val %)) (frequencies child-weights))))]
       (if (and (not (contains? (set child-weights) nil))
                (= 2 (count (set child-weights)))
                (= (count (:children process-tree)) (count (set grandchild-weights))))
           {:tree (first (filter #(= bad-weight (:total-weight %))
                                 (:children process-tree)))
            :weights (frequencies child-weights)
            :good    good-weights
            :bad     bad-weight}
           (pmap walk-tree (:children process-tree)))))

(defn advent7-2 [process-list]
  (let [tree   (build-tree process-list)
        ans    (walk-tree (:tree (walk-tree (:tree (walk-tree (sum-children tree))))))]
    (+ (:weight (:tree ans))
       (- (:good ans) (:bad ans)))))
