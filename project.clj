(defproject advent-of-code "0.1.0-SNAPSHOT"
  :description "FIXME: write description"
  :url "http://example.com/FIXME"
  :license {:name "Eclipse Public License"
            :url "http://www.eclipse.org/legal/epl-v10.html"}
  :dependencies [[org.clojure/clojure "1.10.1"]
                 [org.clojure/math.combinatorics "0.1.4"]
                 [eftest "0.5.3"]
                 [criterium "0.4.4"]
                 ;; [uncomplicate/neanderthal "0.18.0"
                 ;;   :exclusions [uncomplicate/clojurecuda
                 ;;                org.jcuda/jcuda-natives
                 ;;                org.jcuda/jcublas-natives]]
                 ;; [uncomplicate/clojurecl "0.8.0"]
                 [aysylu/loom "1.0.2"]])
;  :plugins [[lein-with-env-vars "0.1.0"]])
;  :env-vars {:DYLD_LIBRARY_PATH "/opt/intel/mkl/lib:/opt/intel/lib"})

