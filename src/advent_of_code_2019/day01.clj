(ns advent-of-code-2019.day01
  (:require [clojure.string :as s]))

(defn parse-masses [masses]
  (map read-string (s/split-lines masses)))

(defn determine-fuel [mass]
  (- (int (Math/floor (/ mass 3))) 2))

(defn determine-fuel-fuel [mass acc]
  (if (< (determine-fuel mass) 1)
    acc
    (recur
     (determine-fuel mass)
     (+ acc (determine-fuel mass)))))

(defn sum-fuel [fuel-parts]
  (reduce + fuel-parts))

(defn answer1 [input]
  (sum-fuel (map determine-fuel (parse-masses input))))

(defn answer2 [input]
  (sum-fuel (map #(determine-fuel-fuel % 0) (parse-masses input))))
