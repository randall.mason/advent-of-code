(ns advent-of-code-2017.day15
  (:require [clojure.string :as s]
            [clojure.pprint :as pp]))

(defn gen-next [factor divisor]
  (fn [x] (rem (* factor x) divisor)))

(defn gen-a [start-number]
  (rest (iterate (gen-next 16807 2147483647) start-number)))

(defn gen-b [start-number]
  (rest (iterate (gen-next 48271 2147483647) start-number)))

(defn format-int [len in]
  (s/join "" (take-last len (pp/cl-format nil (str "~32,'0',B") in))))

(defn judge [seq-a seq-b count matches]
    (filter (fn [pair] (= (first pair) (second pair)))
            (take count (partition 2 (map (partial format-int 16) (interleave seq-a seq-b))))))
