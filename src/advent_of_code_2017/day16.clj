(ns advent-of-code-2017.day16
  (:require [clojure.string :as s]
            [clojure.pprint :as pp]))


(defn generate-dancers [length]
  (mapv char (range 97 (+ 97 length)))) 

(defn parse-moves [moves]
  (->> moves
   s/trim
   (#(s/split % #","))
   (map #(rest (re-matches #"(.)(..?)(?:/(..?))?" %)))))

(defn spin [dancers move]
  (let [spin-len     (bigint (second move))
        dancer-count (count  dancers)]
       (into   (subvec dancers (- dancer-count spin-len))
               (subvec dancers 0 (- dancer-count spin-len)))))
(defn exchange [dancers move]
  (let [dancer-1 (bigint (nth move 1))
        dancer-2 (bigint (nth move 2))]
    (assoc dancers dancer-1 (nth dancers dancer-2) dancer-2 (nth dancers dancer-1))))
(defn partner [dancers move]
  (let [dancer-1 (.indexOf dancers (first (seq (char-array (nth move 1)))))
        dancer-2 (.indexOf dancers (first (seq (char-array (nth move 2)))))]
    (assoc dancers dancer-1 (nth dancers dancer-2) dancer-2 (nth dancers dancer-1))))

(def move-mapper
  {:s spin
   :x exchange
   :p partner})

(defn dance [moves dancers]
  (if  (< 0 (count moves))
    (recur (rest moves) (((keyword (first (first moves))) move-mapper) dancers (first moves)))
    dancers))
