(ns advent-of-code-2017.core-test
  (:require [clojure.test :refer :all]))
            ;; [clojure.string :as s]
            ;; [clojure.core.reducers :as r]
            ;; [advent-of-code-2017.day01 :as day01]
            ;; [advent-of-code-2017.day02 :as day02]
            ;; [advent-of-code-2017.day03 :as day03]
            ;; [advent-of-code-2017.day04 :as day04]
            ;; [advent-of-code-2017.day05 :as day05]
            ;; [advent-of-code-2017.day06 :as day06]
            ;; [advent-of-code-2017.day07 :as day07]
            ;; [advent-of-code-2017.day08 :as day08]
            ;; [advent-of-code-2017.day09 :as day09]
            ;; [advent-of-code-2017.day10 :as day10]
            ;; [advent-of-code-2017.day11 :as day11]
            ;; [advent-of-code-2017.day12 :as day12]
            ;; [advent-of-code-2017.day13 :as day13]
            ;; [advent-of-code-2017.day14 :as day14]
            ;; [advent-of-code-2017.day15 :as day15]
            ;; [advent-of-code-2017.day16 :as day16]
            ;; [advent-of-code-2017.day17 :as day17]
            ;; [advent-of-code-2017.day18 :as day18]
            ;; [advent-of-code-2017.day19 :as day19]
            ;; [advent-of-code-2017.day20 :as day20]
            ;; [advent-of-code-2017.day21 :as day21]
            ;; [advent-of-code-2017.day25 :as day25]))

#_(deftest adven1-1
    (testing "simple"
      #_(is (= 3 (day01/advent1-1 "1122"))))
    (testing "repeats"
      #_(is (= 4 (day01/advent1-1 "1111"))))
    (testing "seq"
      #_(is (= 0 (day01/advent1-1 "1234"))))
    (testing "loop"
      #_(is (= 9 (day01/advent1-1 "91212129"))))
    (testing "answer"
      #_(is (= 1069 (day01/advent1-1 "36743676522426214741687639282183216978128565594112364817283598621384839756628424146779311928318383597235968644687665159591573413233616717112157752469191845757712928347624726438516211153946892241449523148419426259291788938621886334734497823163281389389853675932246734153563861233894952657625868415432316155487242813798425779743561987563734944962846865263722712768674838244444385768568489842989878163655771847362656153372265945464128668412439248966939398765446171855144544285463517258749813731314365947372548811434646381595273172982466142248474238762554858654679415418693478512641864168398722199638775667744977941183772494538685398862344164521446115925528534491788728448668455349588972443295391385389551783289417349823383324748411689198219329996666752251815562522759374542652969147696419669914534586732436912798519697722586795746371697338416716842214313393228587413399534716394984183943123375517819622837972796431166264646432893478557659387795573234889141897313158457637142238315327877493994933514112645586351127139429281675912366669475931711974332271368287413985682374943195886455927839573986464555141679291998645936683639162588375974549467767623463935561847869527383395278248952314792112113126231246742753119748113828843917812547224498319849947517745625844819175973986843636628414965664466582172419197227695368492433353199233558872319529626825788288176275546566474824257336863977574347328469153319428883748696399544974133392589823343773897313173336568883385364166336362398636684459886283964242249228938383219255513996468586953519638111599935229115228837559242752925943653623682985576323929415445443378189472782454958232341986626791182861644112974418239286486722654442144851173538756859647218768134572858331849543266169672745221391659363674921469481143686952478771714585793322926824623482923579986434741714167134346384551362664177865452895348948953472328966995731169672573555621939584872187999325322327893336736611929752613241935211664248961527687778371971259654541239471766714469122213793348414477789271187324629397292446879752673")))))

#_(deftest adven1-2
    (testing "simple"
      #_(is (= 6 (day01/advent1-2 "1212"))))
    (testing "repeats"
      #_(is (= 4 (day01/advent1-2 "123425"))))
    (testing "seq"
      #_(is (= 0 (day01/advent1-2 "1221"))))
    (testing "loop"
       #_(is (= 12 (day01/advent1-2 "123123"))))
    (testing "loop"
       #_(is (= 4 (day01/advent1-2 "12131415"))))
    (testing "answer"
       #_(is (= 1268 (day01/advent1-2 "36743676522426214741687639282183216978128565594112364817283598621384839756628424146779311928318383597235968644687665159591573413233616717112157752469191845757712928347624726438516211153946892241449523148419426259291788938621886334734497823163281389389853675932246734153563861233894952657625868415432316155487242813798425779743561987563734944962846865263722712768674838244444385768568489842989878163655771847362656153372265945464128668412439248966939398765446171855144544285463517258749813731314365947372548811434646381595273172982466142248474238762554858654679415418693478512641864168398722199638775667744977941183772494538685398862344164521446115925528534491788728448668455349588972443295391385389551783289417349823383324748411689198219329996666752251815562522759374542652969147696419669914534586732436912798519697722586795746371697338416716842214313393228587413399534716394984183943123375517819622837972796431166264646432893478557659387795573234889141897313158457637142238315327877493994933514112645586351127139429281675912366669475931711974332271368287413985682374943195886455927839573986464555141679291998645936683639162588375974549467767623463935561847869527383395278248952314792112113126231246742753119748113828843917812547224498319849947517745625844819175973986843636628414965664466582172419197227695368492433353199233558872319529626825788288176275546566474824257336863977574347328469153319428883748696399544974133392589823343773897313173336568883385364166336362398636684459886283964242249228938383219255513996468586953519638111599935229115228837559242752925943653623682985576323929415445443378189472782454958232341986626791182861644112974418239286486722654442144851173538756859647218768134572858331849543266169672745221391659363674921469481143686952478771714585793322926824623482923579986434741714167134346384551362664177865452895348948953472328966995731169672573555621939584872187999325322327893336736611929752613241935211664248961527687778371971259654541239471766714469122213793348414477789271187324629397292446879752673")))
       #_(is (not= 2382 (day01/advent1-2 "36743676522426214741687639282183216978128565594112364817283598621384839756628424146779311928318383597235968644687665159591573413233616717112157752469191845757712928347624726438516211153946892241449523148419426259291788938621886334734497823163281389389853675932246734153563861233894952657625868415432316155487242813798425779743561987563734944962846865263722712768674838244444385768568489842989878163655771847362656153372265945464128668412439248966939398765446171855144544285463517258749813731314365947372548811434646381595273172982466142248474238762554858654679415418693478512641864168398722199638775667744977941183772494538685398862344164521446115925528534491788728448668455349588972443295391385389551783289417349823383324748411689198219329996666752251815562522759374542652969147696419669914534586732436912798519697722586795746371697338416716842214313393228587413399534716394984183943123375517819622837972796431166264646432893478557659387795573234889141897313158457637142238315327877493994933514112645586351127139429281675912366669475931711974332271368287413985682374943195886455927839573986464555141679291998645936683639162588375974549467767623463935561847869527383395278248952314792112113126231246742753119748113828843917812547224498319849947517745625844819175973986843636628414965664466582172419197227695368492433353199233558872319529626825788288176275546566474824257336863977574347328469153319428883748696399544974133392589823343773897313173336568883385364166336362398636684459886283964242249228938383219255513996468586953519638111599935229115228837559242752925943653623682985576323929415445443378189472782454958232341986626791182861644112974418239286486722654442144851173538756859647218768134572858331849543266169672745221391659363674921469481143686952478771714585793322926824623482923579986434741714167134346384551362664177865452895348948953472328966995731169672573555621939584872187999325322327893336736611929752613241935211664248961527687778371971259654541239471766714469122213793348414477789271187324629397292446879752673")))))

#_(deftest adven2-1
    (testing "example"
       #_(is (= 18 (day02/advent2-1 "5\t1\t9\t5\n7\t5\t3\n2\t4\t6\t8"))))
    (testing "final"
       #_(is (= 54426 (day02/advent2-1 (slurp "resources/2017/advent2-1.txt"))))))

#_(deftest adven2-2
    (testing "example"
       #_(is (= 9 (day02/advent2-2 "5 9 2 8\n9 4 7 3\n3 8 6 5")))
       #_(is (= 333 (day02/advent2-2 (slurp "resources/2017/advent2-1.txt"))))))

#_(deftest adven3-1
    (testing "square"
       #_(is (= 3 (day03/advent3-1 12)))
       #_(is (= 2 (day03/advent3-1 23)))
       #_(is (= 2 (day03/advent3-1 3)))
       #_(is (= 0 (day03/advent3-1 1)))
       #_(is (= 31 (day03/advent3-1 1024)))
       #_(is (= 326 (day03/advent3-1 361527)))))

#_(deftest adven3-2
    #_(testing "square")
      #_(is (= 25  (day03/advent3-2 23)))
      #_(is (= 25  (day03/advent3-2 24)))
      #_(is (= 26  (day03/advent3-2 25)))
      #_(is (= 133 (day03/advent3-2 125)))
      #_(is (= 806 (day03/advent3-2 750)))
      #_(is (= 363010 (day03/advent3-2 361527))))

#_(deftest adven4-1
    (testing "no duplicate words"
      #_(is (= true (day04/advent4-1 "aa bb cc dd ee")))
      #_(is (= false (day04/advent4-1 "aa bb cc dd aa")))
      #_(is (= true (day04/advent4-1 "aa bb cc dd aaa")))
      #_(is (= 337  (count (filter (partial = true) (map day04/advent4-1 (s/split-lines (slurp "resources/2017/advent4-1.txt")))))))))

#_(deftest adven4-2
    (testing "no anagrams"
      #_(is (= true (day04/advent4-2 "abcde fghij")))
      #_(is (= false (day04/advent4-2 "abcde xyz ecdab")))
      #_(is (= true (day04/advent4-2 "a ab abc abd abf abj")))
      #_(is (= true (day04/advent4-2 "iiii oiii ooii oooi oooo")))
      #_(is (= false (day04/advent4-2 "oiii ioii iioi iiio")))
      #_(is (= 231  (count (filter (partial = true) (map day04/advent4-2 (s/split-lines (slurp "resources/2017/advent4-1.txt")))))))))

#_(deftest adven5-1
    (testing "instruction maze"
      #_(is (= 5 (day05/advent5-1 "0\n3\n0\n1\n-3")))
      #_(is (= 351282 (day05/advent5-1 (slurp "resources/2017/advent5-1.txt"))))))

#_(deftest adven5-2
    (testing "instruction maze"
      #_(is (= 10 (day05/advent5-2 "0\n3\n0\n1\n-3")))
      #_(is (= 24568703 (day05/advent5-2 (slurp "resources/2017/advent5-1.txt"))))))

#_(deftest adven6-1
    (testing "memory redistribution"
     #_(is (= 5042 (first (day06/advent6-1 (slurp "resources/2017/advent6-1.txt")))))
     #_(is (= 5 (first (day06/advent6-1 "0\t2\t7\t0"))))))

#_(deftest adven6-2
    (testing "memory redistribution"
     #_(is (= 1086 (second (day06/advent6-1 (slurp "resources/2017/advent6-1.txt")))))
     #_(is (= 4 (second (day06/advent6-1 "0\t2\t7\t0"))))))

#_(deftest adven7
    (testing "stack part 1"
      #_(is (= "tknk" (day07/advent7-1 (slurp "resources/2017/advent7-1.example.txt"))))
      #_(is (= "cyrupz" (day07/advent7-1 (slurp "resources/2017/advent7-1.txt"))))
      #_(is (= "taylor" (day07/advent7-1 (slurp "resources/2017/day7.input")))))

    (testing "stack part 2 - weight"
      #_(is (= 60 (day07/advent7-2 (slurp "resources/2017/advent7-1.example.txt"))))
      #_(is (= "tknk" (day07/advent7-2 (slurp "resources/2017/advent7-1.txt"))))
      #_(is (< 485 (day07/advent7-2 (slurp "resources/2017/advent7-1.txt"))))
      #_(is (= 193 (day07/advent7-2 (slurp "resources/2017/advent7-1.txt"))))
      #_(is (= "taylor" (day07/advent7-2 (slurp "resources/2017/day7.input"))))))

#_(deftest advent8
    (testing "largest value after execution"
      #_(is (= 1 (day08/advent8-1 (slurp "resources/2017/advent8.example.txt"))))
      #_(is (= 5946 (day08/advent8-1 (slurp "resources/2017/advent8.txt")))))
    (testing "largest value during execution"
      #_(is (= 10 (day08/advent8-2 (slurp "resources/2017/advent8.example.txt"))))
      #_(is (= 6026N (day08/advent8-2 (slurp "resources/2017/advent8.txt"))))))

#_(deftest advent9
    (testing "garbage deletion"
      #_(is (= "" (s/trim (day09/garbage-deletion (slurp "resources/2017/advent9.example.garbage.txt"))))))
    (testing "group counting"
      #_(is (= [1 3 3 6 1 1 5 2] (map day09/count-groups (s/split-lines (day09/garbage-deletion (slurp "resources/2017/advent9.example.whole-streams.txt")))))))
    (testing "group scoreing"
      #_(is (= '(1 6 5 16 1 9 9 3) (map day09/score (s/split-lines (day09/garbage-deletion (slurp "resources/2017/advent9.example.scores.txt"))))))
      #_(is (= 13154 (first (map day09/score (s/split-lines (day09/garbage-deletion (slurp "resources/2017/advent9.txt"))))))))
    (testing "count garbage"
      #_(is (> 7825 (:garbage-deleted (day09/garbage-deletion-with-count (slurp "resources/2017/advent9.txt")))))
      #_(is (= 6369 (:garbage-deleted (day09/garbage-deletion-with-count (slurp "resources/2017/advent9.txt")))))
      #_(is (= 0 (:garbage-deleted (day09/garbage-deletion-with-count "<>"))))))

#_(defn bhauman-reverse-section [x length pos]
    (let [idx (mapv #(mod % (count x))
                    (range pos (+ pos length)))
          vs (r/reduce #(cons (get x %2) %1) '() idx)]
      (persistent!
       (loop [[id & idx'] idx
              [v & vs']    vs
              x          (transient x)]
         (if id
           (recur idx' vs' (assoc! x id v))
           x)))))

#_(deftest advent10
    (testing "rotations"
      #_(is (= [2 1 0 3 4] (day10/reverse-section [0 1 2 3 4] 3 0)))
      #_(is (= [4 3 0 1 2] (day10/reverse-section [2 1 0 3 4] 4 3)))
      #_(is (= [4 3 0 1 2] (day10/reverse-section [4 3 0 1 2] 1 3)))
      #_(is (= [3 4 2 1 0] (day10/reverse-section [4 3 0 1 2] 5 1)))
      #_(is (= [1 0 5 4 2 8 6 7 3 9] (day10/reverse-section [1 0 5 4 9 3 7 6 8 2] 6 4)))
      #_(is (= [1 4 3 8 2 9 0 5 7 6] (day10/reverse-section [9 2 8 3 4 1 6 5 7 0] 8 9)))
      #_(is (= [2 1 0 3 4] (bhauman-reverse-section [0 1 2 3 4] 3 0)))
      #_(is (= [4 3 0 1 2] (bhauman-reverse-section [2 1 0 3 4] 4 3)))
      #_(is (= [4 3 0 1 2] (bhauman-reverse-section [4 3 0 1 2] 1 3)))
      #_(is (= [3 4 2 1 0] (bhauman-reverse-section [4 3 0 1 2] 5 1)))
      (let [random-list (shuffle (range 10))
            random-len  (rand-int 10)
            random-pos  (rand-int 10)]
        #_(is (= [(bhauman-reverse-section random-list random-len random-pos) random-list random-len random-pos]
               [(day10/reverse-section   random-list random-len random-pos) random-list random-len random-pos]))))
    (testing "short example multipy first two after hash"
      #_(is (= 12 (reduce * (take 2 (day10/knot-hash 5 "3,4,1,5")))))
      #_(is (= 256 (count (set (day10/knot-hash 256 (s/trim (slurp "resources/2017/advent10.txt")))))))
      #_(is (< 12180 (reduce * (take 2 (day10/knot-hash 256 (s/trim (slurp "resources/2017/advent10.txt")))))))
      #_(is (< 38220 (reduce * (take 2 (day10/knot-hash 256 (s/trim (slurp "resources/2017/advent10.txt")))))))
      #_(is (> 63252 (reduce * (take 2 (day10/knot-hash 256 (s/trim (slurp "resources/2017/advent10.txt")))))))
      #_(is (= 48705 (reduce * (take 2 (day10/knot-hash 256 (s/trim (slurp "resources/2017/advent10.txt"))))))))
    (testing "new input"
      #_(is (= [49,44,50,44,51] (day10/parse-input "1,2,3")))
      #_(is (= 64 (day10/xor-bytes [65 27 9 1 4 3 40 50 91 7 6 0 2 5 68 22])))
      #_(is (= "4007ff" (day10/bytes-to-string [64, 7, 255])))
      #_(is (= "a2582a3a0e66e6e86e3812dcb672a272" (bytes-to-string (day10/multi-round-knot-hash 64 ""))))
      #_(is (= "33efeb34ea91902bb2f59c9920caa6cd" (bytes-to-string (day10/multi-round-knot-hash 64 "AoC 2017"))))
      #_(is (= "3efbe78a8d82f29979031a4aa0b16a9d" (bytes-to-string (day10/multi-round-knot-hash 64 "1,2,3"))))
      #_(is (= "63960835bcdc130f0b66d7ff4f6a5a8e" (bytes-to-string (day10/multi-round-knot-hash 64 "1,2,4"))))
      #_(is (= "1c46642b6f2bc21db2a2149d0aeeae5d" (bytes-to-string (day10/multi-round-knot-hash 64 (s/trim (slurp "resources/2017/advent10.txt"))))))))

#_(deftest adven11
    (testing "count-steps"
      #_(is (= 3 (day11/advent11-1 (day11/parse-path "ne,ne,ne"))))
      #_(is (= 0 (day11/advent11-1 (day11/parse-path "ne,ne,sw,sw"))))
      #_(is (= 2 (day11/advent11-1 (day11/parse-path "ne,ne,s,s"))))
      #_(is (= 3 (day11/advent11-1 (day11/parse-path "se,sw,se,sw,sw"))))
      #_(is (= 685 (day11/advent11-1 (day11/parse-path (slurp "resources/2017/advent11.txt"))))))
    (testing "find farthest"
      #_(is (= 3 (day11/advent11-2 (day11/parse-path "ne,ne,ne"))))
      #_(is (= 2 (day11/advent11-2 (day11/parse-path "ne,ne,sw,sw"))))
      #_(is (= 2 (day11/advent11-2 (day11/parse-path "ne,ne,s,s"))))
      #_(is (= 3 (day11/advent11-2 (day11/parse-path "se,sw,se,sw,sw"))))
      #_(is (= 1457 (day11/advent11-2 (day11/parse-path (slurp "resources/2017/advent11.txt")))))))

#_(deftest adven12-1
    (testing "connectedness"
      #_(is (= 6 (day12/advent12-1 (day12/load-pipes (slurp "resources/2017/advent12.example.txt")))))
      #_(is (= 1500 (day12/advent12-1 (day12/load-pipes (slurp "resources/2017/advent12.txt"))))))
    (testing "groups"
      #_(is (= 2 (day12/advent12-2 (day12/load-pipes (slurp "resources/2017/advent12.example.txt")))))
      #_(is (= 2 (day12/advent12-2 (day12/load-pipes (slurp "resources/2017/advent12.txt")))))))

#_(deftest advent13
    (testing "firewall cost"
      #_(is (= 24 (day13/advent13-1 (day13/load-firewall (slurp "resources/2017/advent13.example.txt")))))))

#_(deftest advent14
    (testing "diskimage"
      #_(is (= [10 0 12 2 0 1 7] (day14/hex-string-to-byte-array "a0c2017")))
      #_(is (= "1010000011000010000000010111" (day14/bytes-to-binary-string (day14/hex-string-to-byte-array "a0c2017"))))
      #_(is (= 8108 (day14/count-bytes (day14/make-grid "flqrgnkx-"))))
      #_(is (= 8194 (day14/count-bytes (day14/make-grid "uugsqrei-"))))
      #_(is (= (take 128 (repeat 128)) (map count (day14/make-grid "flqrgnkx-")))))
    (testing "contiguous regios"
      #_(is (= 1242 (day14/count-contiguous (day14/make-grid "flqrgnkx-"))))
      #_(is (= 1141 (day14/count-contiguous (day14/make-grid "uugsqrei-"))))))

#_(deftest advent15
    (testing "first 5"
      (is (= [1092455 1181022009 245556042 1744312007 1352636452] (take 5 (day15/gen-a 65))))
      (is (= [430625591 1233683848 1431495498 137874439 285222916] (take 5 (day15/gen-b 8921)))))
    (testing "binary representation"
      (is (= "00000000000100001010101101100111" (day15/format-int 32 (first (day15/gen-a 65)))))
      (is (= "00011001101010101101001100110111" (day15/format-int 32 (first (day15/gen-b 8921))))))
    (testing "binary last 16"
      (is (= 1  (count (day15/judge (day15/gen-a 65) (day15/gen-b 8921) 5 0))))
      #_(is (= 588 (count (day15/judge (day15/gen-a 65) (day15/gen-b 8921) 40000000 0))))
      #_(is (= 638 (count (day15/judge (day15/gen-a 289) (day15/gen-b 629) 40000000 0)))))
    (testing "picky generators"
      (is (= [1352636452 1992081072 530830436 1980017072 740335192] 
             (take 5 (filter #(= 0 (rem % 4))  (day15/gen-a 65))))
          (= [1233683848    862516352   1159784568   1616057672   412269392]
             (take 5 (filter #(= 0 (mod % 8))  (day15/gen-b 8921)))))
      #_(is (= 309 (count (day15/judge
                           (filter #(= 0 (rem % 4)) (day15/gen-a 65))
                           (filter #(= 0 (mod % 8)) (day15/gen-b 8921))
                           5000000 0))))
      #_(is (= 343 (count (day15/judge
                           (filter #(= 0 (rem % 4)) (day15/gen-a 289))
                           (filter #(= 0 (mod % 8)) (day15/gen-b 629))
                           5000000 0))))))

#_(deftest advent16)
  (testing "dance moves"
    #_(is (= [\b \a \e \d \c]
           (day16/dance (day16/parse-moves (slurp "resources/2017/advent16.example.txt"))
                        (day16/generate-dancers 5))))
    #_(is (= [\c \g \p \f \h \d \n \a \m \b \e \k \j \i \o \l]
           (day16/dance (day16/parse-moves (slurp "resources/2017/advent16.txt"))
                        (day16/generate-dancers 16))))
    #_(is (= [\c \e \a \d \b]
           (nth (iterate
                 (partial day16/dance (day16/parse-moves (slurp "resources/2017/advent16.example.txt")))
                 (day16/generate-dancers 5))
                2)))
    #_(is (= [\c \g \p \f \h \d \n \a \m \b \e \k \j \i \o \l]
           (nth (iterate
                  (partial day16/dance (day16/parse-moves (slurp "resources/2017/advent16.txt")))
                  (day16/generate-dancers 16))
                1000000000N))))

#_(deftest advent21)
 (testing "similarity"
    #_(is (= [[ 0 1 0][0 0 1][1 1 1]] (day21/parse ".#./..#/###")))
    #_(is (= #{".#./..#/###"} (into #{} (mapv day21/normalize [".#./#../###" "#../#.#/##." "###/..#/.#." ".#./..#/###"])))))
 (testing "example-rules"
    #_(is (= "##.##./#..#../....../##.##./#..#../......" (day21/generate (slurp "resources/2017/advent21.example.txt") ".#./..#/###" 2)))
    #_(is (= 12 (day21/count-pixels "##.##./#..#../....../##.##./#..#../......")))
    #_(is (= 12 (day21/count-pixels (day21/generate (day21/parse-rules (slurp "resources/2017/advent21.example.txt")) ".#./..#/###" 2)))))

#_(deftest advent24)
  (testing "strongest"
    #_(is (= 31 (day24/strongest (day24/build-bridges (slurp "resources/2017/advent24.example.txt")))))
    #_(is (= 1695 (day24/strongest (day24/build-bridges (slurp "resources/2017/advent24.txt"))))))
  (testing "strongest-longest"
    #_(is (= 19 (day24/strongest-longest (day24/build-bridges (slurp "resources/2017/advent24.example.txt")))))
    #_(is (= 1673 (day24/strongest-longest (day24/build-bridges (slurp "resources/2017/advent24.txt"))))))
    ; had to crank up :jvm-opts to ["-Xmx8g" "-server"]

#_(deftest advent25
    (testing "checksum"
      #_(is (= 3 (day25/diagnostic-checksum (day25/run-computer (day25/parse-computer (slurp "resources/2017/advent25.example.txt"))))))
      #_(is (= 3745 (day25/diagnostic-checksum (day25/run-computer (day25/parse-computer (slurp "resources/2017/advent25.txt"))))))))
