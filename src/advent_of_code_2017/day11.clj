(ns advent-of-code-2017.day11
  (:require [clojure.string :as s]
            [clojure.pprint :as pp]
            [clojure.walk :as w]))

(deftype hex [^Integer north-south
              ^Integer northwest-southeast
              ^Integer northeast-southwest])

(defmethod print-method hex [v ^java.io.Writer w]
  (.write w (str (.north-south v) " north, " (.northwest-southeast v) " northwest, " (.northeast-southwest v) " northeast")))

(defn plus
 ([] (hex. 0 0 0))
 ([^hex h1]
  h1)
 ([^hex h1 ^hex h2]
  (hex. (+ (.north-south h1) (.north-south h2))
        (+ (.northwest-southeast h1) (.northwest-southeast h2))
        (+ (.northeast-southwest h1) (.northeast-southwest h2))))
 ([^hex h1 ^hex h2 & more]
  (reduce plus (plus h1 h2) more)))

(def equiv
  {(hex. -1  0  0) (hex.  0  1  1)
   (hex.  1  0  0) (hex.  0 -1 -1)
   (hex.  0  1  0) (hex. -1  0  1)
   (hex.  0 -1  0) (hex.  1  0 -1)
   (hex.  0  0  1) (hex. -1  1  0)
   (hex.  0  0 -1) (hex.  1 -1  0)})


(defn magnitude [^hex h]
  (+ (Math/abs (.north-south h))
     (Math/abs (.northwest-southeast h))
     (Math/abs (.northeast-southwest h))))

(defn collapse [^hex h]
 (let [transformations (map-indexed #(vector %1 (magnitude (plus h (val %2) (key %2))) %2) equiv)]
  (if (< (magnitude h)
         (apply min (map second transformations)))
      h
      (recur (apply plus h (nth (first (filter #(> (magnitude h) (second %))
                                               transformations)) 2))))))

(def m-collapse
  (memoize collapse))

(def conversion
  {:n  (hex.  1  0  0)
   :s  (hex. -1  0  0)
   :nw (hex.  0  1  0)
   :se (hex.  0 -1  0)
   :ne (hex.  0  0  1)
   :sw (hex.  0  0 -1)})

(defn parse-path [path-string]
  (-> path-string
      s/trim
     (s/split #",")
     (#(map (comp conversion keyword) %))))

(defn advent11-1 [path]
  (-> path
   (#(reduce plus %))
   m-collapse
   magnitude))

(defn advent11-2 [path]
  (apply max (map #(advent11-1 (take % path)) (range 0 (inc (count path))))))

(defn load-pipes [pipe-string]
  (println pipe-string)
  (->> pipe-string
    s/split-lines
    (map s/trim)
    (map-indexed #(into {} {:id %1 :connections (s/split (nth (re-find #"^(.*) <-> (.*)$" %2) 2) #", ")}))))

; (defn parse-element [element-string]
;   (let [[root, children] (s/split element-string #" -> ")
;         children         (if children (s/split children #"\, "))
;         [_ name weight]        (re-find #"^(.*) \((.*)\)$" root)]
;     {:process name :weight (bigint weight) :children children}))
;
; (defn build-tree [process-list]
;   (let [elements (->> process-list
;                    s/split-lines
;                    (map s/trim)
;                    (map parse-element))
;         leaves   (apply vector (filter #(nil? (:children %)) elements))
;         roots    (apply vector (filter #(:children %) elements))]
;     (build-tree-from-roots-leaves roots leaves)))

(defn find-pipes [connections])

(defn advent12-1 [pipes]
  pipes)
