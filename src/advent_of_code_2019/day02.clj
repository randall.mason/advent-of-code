(ns advent-of-code-2019.day02
  (:require [clojure.string :as s]))

(defn parse-instructions [program]
  (into [] (map read-string (s/split program #","))))

(def instructions
  {1 +
   2 *
   99 :halt})

(defn alter-instruction [instructions noun verb]
  (assoc (assoc instructions 1 noun) 2 verb))

(defn execute-instruction [input instruction]
  (assoc input (get input (inc (inc (inc instruction))))
           ((instructions (get input instruction))
            (get input (get input (inc instruction)))
            (get input (get input (inc (inc instruction)))))))

(defn answer1 [input instruction]
  (if (= 99 (get input instruction))
      input
      (recur (execute-instruction input instruction)
             (+ 4 instruction))))
    
(defn answer2 [input goal]
  (loop [x 0
         y 0]
    (if (= goal (first (answer1 (alter-instruction input x y) 0)))
      (do
        (println [x y])
        (+ (* 100 x) y))
      (recur (mod (inc x) 100) (+ y (quot x 99))))))